package com.example.user.Birds.TestBirds;

import android.util.Log;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by User on 18.04.2016.
 */

public class TestBirds {

    //region Свойства

    //region Параметры теста

    //количество очков
    private int points;

    //количество правильных ответов
    private int rightAnswers;

    //количество попаданий
    private int kolHits;

    //число появляющееся в центре экрана
    private int targetNumber;

    //максимальное количество птиц
    private int kolBirdsMax;

    //текущее количество птиц
    private int kolBirdsCurrent;

    //максимальное количество дистракторов
    private int kolDistractorsMax;

    //текущее количество дистракторов
    private int kolDistractorsCurrent;

    //номер фоновой картинки
    private int background;

    //endregion

    //region Рандом
    //рандомайзер

    private Random random = new Random();

    //endregion

    //region Параметры экрана

    //ширина экрана
    private int displayWidth;

    //высота экрана
    private int displayHeight;

    //endregion

    //region Координаты

    //координата птицы, Х
    private int birdPosX;

    //координата птицы, Y
    private int birdPosY;

    //координата дистрактора, Х
    private int distractorPosX;

    //координата дистрактора, Y
    private int distractorPosY;

    //список координат дистракторов
    private ArrayList<Integer> distractorsCoord = new ArrayList<Integer>();

    //endregion

    //region Временные параметры

    //время между показами птиц
    private int timeBetweenShowBird;

    //максимальное время показа птицы
    private int timeMax;

    //минимальное время показа птицы
    private int timeMin;

    //шаг изменения времени показа птицы
    private int timeStep;

    //текущее время показа птицы
    private int timeCurrent;

    //endregion

    //region Остальные параметры

    //расстояние от центра птицы до точки касания
    private int distance;

    //имя пользователя
    private String name;

    //endregion

    //endregion

    //region Методы

    //region Конструктор

    public TestBirds(){
        this.background = 1;
        this.timeMax = 1000;
        this.timeMin = 200;
        this.timeStep = 100;
        this.timeCurrent = this.timeMax;
        this.kolBirdsMax = 10;
        this.points = 0;
        this.kolBirdsCurrent = 0;
        this.kolDistractorsMax = 3;
        this.kolDistractorsCurrent = 0;
        this.timeBetweenShowBird = 700;
        this.targetNumber = 1;
        this.rightAnswers = 0;
    }

    //endregion

    //region Set-функции

    public void setTimeBetweenShowBird(int timeBetweenShowBird){
        this.timeBetweenShowBird = timeBetweenShowBird;
    }

    public void setTargetNumber(int targetNumber){
        this.targetNumber = targetNumber;
    }

    public void setRightAnswers(int rightAnswers){
        this.rightAnswers = rightAnswers;
    }

    public void setTimeMax(int timeMax){
        this.timeMax = timeMax;
    }

    public void setTimeMin(int timeMin){
        this.timeMin = timeMin;
    }

    public void setTimeStep(int timeStep){
        this.timeStep = timeStep;
    }

    public void setTimeCurrent(int timeCurrent){
        this.timeCurrent = timeCurrent;
    }

    public void setBackground(int background){
        this.background = background;
    }

    public void setPoints(int points){
        this.points = points;
    }

    public void setKolHits(int kolHits){
        this.kolHits = kolHits;
    }

    public void setKolBirdsMax(int kolBirdsMax){
        this.kolBirdsMax = kolBirdsMax;
    }

    public void setKolBirdsCurrent(int kolBirdsCurrent){
        this.kolBirdsCurrent = kolBirdsCurrent;
    }

    public void setBirdPosX(int birdPosX){
        this.birdPosX = birdPosX;
    }

    public void setBirdPosY(int birdPosY){
        this.birdPosY = birdPosY;
    }

    public void setKolDistractorsMax(int kolDistractorsMax){
        this.kolDistractorsMax = kolDistractorsMax;
    }

    public void setKolDistractorsCurrent(int kolDistractorsCurrent){
        this.kolDistractorsCurrent = kolDistractorsCurrent;
    }

    public void setDisplayWidth(int displayWidth){
        this.displayWidth = displayWidth;
    }

    public void setDisplayHeight(int displayHeight){
        this.displayHeight = displayHeight;
    }

    public void setDistractorPosX(int distractorPosX){
        this.distractorPosX = distractorPosX;
    }

    public void setDistractorPosY(int distractorPosY){
        this.distractorPosY = distractorPosY;
    }

    public void setName(String name){
        this.name = name;
    }

    public void setTimeCurrent(){
        if(this.kolDistractorsCurrent < 1){
            this.timeCurrent = this.timeMax;
        }
    }

    //endregion

    //region Get-функции

    public int getTimeMax(){
        return this.timeMax;
    }

    public int getTimeMin(){
        return this.timeMin;
    }

    public int getDistance(){
        return this.distance;
    }

    public int getTimeStep(){
        return this.timeStep;
    }

    public int getTimeCurrent(){
        return this.timeCurrent;
    }

    public int getTimeBetweenShowBird(){
        return this.timeBetweenShowBird;
    }

    public int getBackground(){
        return this.background;
    }

    public int getPoints(){
        return this.points;
    }

    public int getRightAnswers(){
        return this.rightAnswers;
    }

    public int getTargetNumber(){
        return this.targetNumber;
    }

    public int getKolBirdsMax(){
        return this.kolBirdsMax;
    }

    public int getKolBirdsCurrent(){
        return this.kolBirdsCurrent;
    }

    public int getBirdPosX(){
        return this.birdPosX;
    }

    public int getBirdPosY(){
        return this.birdPosY;
    }

    public int getKolDistractorsMax(){
        return this.kolDistractorsMax;
    }

    public int getKolDistractorsCurrent(){
        return this.kolDistractorsCurrent;
    }

    public int getDisplayWidth(){
        return this.displayWidth;
    }

    public int getDisplayHeight(){
        return this.displayHeight;
    }

    public int getDistractorPosX(){
        return this.distractorPosX;
    }

    public int getDistractorPosY() {
        return this.distractorPosY;
    }

    public int getKolHits(){
        return this.kolHits;
    }

    public String getName(){
        return this.name;
    }

    public ArrayList<Integer> getDistractorsCoord(){
        return this.distractorsCoord;
    }

    //endregion

    //region Вычисления

    public double defineKoeffDist(){

        if(this.distance == 1){
            return 0.5;
        }
        else
            if(this.distance == 2){
                return 0.833;
            }
        else
            return 1.0;
    }

    public void calcPoints(int time, int koeffBird, int koeffNumber) {
        double value;
        double koeffDist = defineKoeffDist();

        value = koeffBird * (1.0 / this.distance * 2000 * koeffDist) +
                koeffNumber * (5.0 * 100.0);

        this.points = (int) (value);
    }

    public void randCoordinateBird(int widthImage, int heightImage){
        int num;

        num = this.random.nextInt(2);
        //для левой части экрана
        if(num == 0){
            this.birdPosX = random.nextInt(this.displayWidth / 4 - widthImage);
            this.birdPosY = random.nextInt(this.displayHeight - heightImage - 100) + 50;
        }
        //для правой части экрана
        else{
            this.birdPosX = random.nextInt(this.displayWidth / 4 - widthImage - 80) + 3 * this.displayWidth / 4;
            this.birdPosY = random.nextInt(this.displayHeight - heightImage - 50);
        }

        Log.d("CoordinatesBirds", this.birdPosX + "," + this.birdPosY);
    }

    public void randCoordinateDistractor(int widthImage, int heightImage){
        int num;
        boolean flag;

        while(true) {
            num = this.random.nextInt(2);

            if(this.distractorsCoord.isEmpty() == false){
                if(num == this.distractorsCoord.get(this.distractorsCoord.size() - 1)){
                    num = num * (-1) + 1;
                }
            }

            flag = true;
            //для левой части экрана
            if (num == 0) {
                this.distractorPosX = this.random.nextInt(this.displayWidth / 4 - widthImage);
                this.distractorPosY = this.random.nextInt(this.displayHeight - heightImage - 100) + 50;
            }
            //для правой части экрана
            else {
                this.distractorPosX = this.random.nextInt(this.displayWidth / 4 - widthImage - 80) + 3 * this.displayWidth / 4;
                this.distractorPosY = this.random.nextInt(this.displayHeight - heightImage - 50);
            }
            //проверка, чтобы дистрактор не заходил на птичку и на сам дистрактор
            if(rand(widthImage, heightImage, this.birdPosX, this.birdPosY) == true) {
                for(int i = 0; i < this.distractorsCoord.size() && flag; i+= 3){

                    if(rand(widthImage, heightImage, this.distractorsCoord.get(i), this.distractorsCoord.get(i + 1)) == false){
                        flag = false;
                    }
                }

                if(flag == true){
                    break;
                }

            }
        }

        this.distractorsCoord.add(this.distractorPosX);
        this.distractorsCoord.add(this.distractorPosY);
        this.distractorsCoord.add(num);

    }

    public boolean rand(int widthImage, int heightImage, int posX, int posY){

        int crack = 100;
        Log.d("CoordinatesBirds", this.birdPosX + "," + this.birdPosY);
        Log.d("CoordinatesDist", this.distractorPosX + "," + this.distractorPosY);
        if(this.distractorPosX >= (posX - crack) &&
                this.distractorPosX <= (posX + widthImage / 4 + crack)
                && this.distractorPosY >= (posY - crack) &&
                this.distractorPosY <= (posY + heightImage / 4 + crack) ||
                this.distractorPosX + widthImage / 4 >= (posX - crack) &&
                this.distractorPosX + widthImage / 4 <= (posX + widthImage / 4 + crack)
                && this.distractorPosY + heightImage / 4 >= (posY - crack) &&
                this.distractorPosY + heightImage / 4 <= (posY + heightImage / 4 + crack)){
                Log.d("Random", "Inside");
            return false;
        }
        else{
            Log.d("Random", "Outside");
            return true;
        }
    }

    public void randomTargetNumber(){
        this.targetNumber = this.random.nextInt(5) + 1;
    }

    //endregion

    //region Update-функции

    public void updateBackground(){

        this.background++;
        if(this.background > 4){
            this.background = 1;
        }
    }

    public void updateTimeCurrent(){

        this.timeCurrent -= this.timeStep;
        if(this.timeCurrent < this.timeMin)
            this.timeCurrent = this.timeMin;
    }

    public void updateKolDistractorsCurrent(){
        this.kolDistractorsCurrent++;
        if(this.kolDistractorsCurrent > this.kolDistractorsMax)
            this.kolDistractorsCurrent = this.kolDistractorsMax;
    }

    public void updateKolBirdsCurrent(){
        this.kolBirdsCurrent++;
    }

    public void updateRightAnswers(){
        this.rightAnswers++;
    }

    public void updateKolHits(){
        this.kolHits++;
    }

    //endregion

    //region Проверки

    public boolean checkClickBird(float touchX, float touchY, int widthImage, int heightImage){

        Log.d("dim", widthImage + "," + heightImage);

        double dist = (touchX - 64 - (this.birdPosX + widthImage / 4)) * (touchX - 64 - (this.birdPosX + widthImage / 4)) +
                (touchY - 64 - (this.birdPosY + heightImage / 4)) * (touchY - 64 -  (this.birdPosY + heightImage / 4));
        dist = Math.sqrt(dist);

        this.distance = (int)dist;
        if(this.distance == 0){
            this.distance = 1;
        }
        int intTouchX = (int) touchX, intTouchY = (int) touchY, x_X0, y_Y0, radius = 50;

        x_X0 = intTouchX - 64 - (this.birdPosX + widthImage / 4);
        y_Y0 = intTouchY - 64 - (this.birdPosY + heightImage / 4);
        if(x_X0 * x_X0 + y_Y0 * y_Y0 <= radius * radius){
            return true;
        }

        return false;
    }

    //endregion

    //endregion
}

