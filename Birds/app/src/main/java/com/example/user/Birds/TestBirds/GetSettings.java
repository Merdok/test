package com.example.user.Birds.TestBirds;

import android.util.Log;
import org.apache.http.NameValuePair;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import java.util.ArrayList;
import java.util.List;

public class GetSettings extends Request {

    //region Свойства

    private int kolStim;

    private int kolDist;

    private int timeMax;

    private int timeMin;

    private int timeStep;

    private int timeBetweenStim;

    private List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);

    //endregion

    //region Методы

    @Override
    public String createRequest() {
        try {
            //создаем запрос на сервер
            DefaultHttpClient hc = new DefaultHttpClient();
            ResponseHandler<String> res = new BasicResponseHandler();
            //создаем post запрос
            HttpPost postMethod = new HttpPost(URL);
            //передаем параметры
            postMethod.setEntity(new UrlEncodedFormEntity(nameValuePairs, HTTP.UTF_8));
            //получаем ответ от сервера
            response = hc.execute(postMethod, res);

            nameValuePairs.clear();

            Log.d("answer", response);

            return response;

        } catch (Exception e) {
            Log.d("Exp=", e + "");
            return "error";
        }
    }

    @Override
    public String getResult(String result) {
        ArrayList<Integer> indexColon = new ArrayList<>();
        String string;
        for(int i = 0; i < result.length(); i++){
            if(result.charAt(i) == ':'){
                indexColon.add(i);
            }
        }

        string = result.substring(0, indexColon.get(0));
        setKolStim(Integer.parseInt(string));
        Log.d("kolStim", string);

        string = result.substring(indexColon.get(0) + 1, indexColon.get(1));
        setKolDist(Integer.parseInt(string));
        Log.d("kolDist", string);

        string = result.substring(indexColon.get(1) + 1, indexColon.get(2));
        setTimeMax(Integer.parseInt(string));
        Log.d("timeMax", string);

        string = result.substring(indexColon.get(2) + 1, indexColon.get(3));
        setTimeMin(Integer.parseInt(string));
        Log.d("timeMin", string);

        string = result.substring(indexColon.get(3) + 1, indexColon.get(4));
        setTimeStep(Integer.parseInt(string));
        Log.d("timeStep", string);

        string = result.substring(indexColon.get(4) + 1, result.length());
        setTimeBetweenStim(Integer.parseInt(string));
        Log.d("timeBetweenStim", string);


        return null;
    }

    public void add(String key, String value){
        nameValuePairs.add(new BasicNameValuePair(key, value));
    }

    public int getKolStim(){
        return this.kolStim;
    }

    public int getKolDist(){
        return this.kolDist;
    }

    public int getTimeMax(){
        return this.timeMax;
    }

    public int getTimeMin(){
        return this.timeMin;
    }

    public int getTimeStep(){
        return this.timeStep;
    }

    public int getTimeBetweenStim(){
        return this.timeBetweenStim;
    }

    public void setKolStim(int kolStim){
        this.kolStim = kolStim;
    }

    public void setKolDist(int kolDist){
        this.kolDist = kolDist;
    }

    public void setTimeMax(int timeMax){
        this.timeMax = timeMax;
    }

    public void setTimeMin(int timeMin){
        this.timeMin = timeMin;
    }

    public void setTimeStep(int timeStep){
        this.timeStep = timeStep;
    }

    public void setTimeBetweenStim(int timeBetweenStim){
        this.timeBetweenStim = timeBetweenStim;
    }

    //endregion
}
