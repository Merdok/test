package com.example.user.Birds.TestBirds;

/**
 * Created by User on 17.05.2016.
 */
public class Checker {

    public boolean isEmptyString(String string){
        return string.isEmpty();
    }

    public boolean isFirstZero(String string) {
        if (string.charAt(0) == '0') {
            return true;
        }
        return false;
    }

    public boolean isDotInString(String string) {

        for (int i = 0; i < string.length(); i++) {
            if (string.charAt(i) == '.') {
                return true;
            }
        }
        return false;
    }

    public boolean isLengthStringMore(String string, int length) {
        if (string.length() < length) {
            return false;
        }
        return true;
    }

    public boolean isAtInString(String string) {
        for (int i = 0; i < string.length(); i++) {
            if (string.charAt(i) == '@') {
                return true;
            }
        }
        return false;
    }

    public boolean isValueInInterval(int value, int leftBorder, int rightBorder){
        if(value < leftBorder || value > rightBorder)
            return false;
        return true;
    }

    public boolean isStringEquals(String string1, String string2){
        if(string1.equals(string2))
            return  true;
        return false;
    }
}
