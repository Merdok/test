package com.example.user.Birds.TestBirds;

import android.content.Context;
import android.content.SharedPreferences;

public class Loader {

    private String value;

    public void load(Context context, String fileName, String key, String initValue){
        SharedPreferences prefs = context.getSharedPreferences(fileName, Context.MODE_PRIVATE);
        this.value = prefs.getString(key, initValue);
    }

    public String getValue(){
        return this.value;
    }
}
