package com.example.user.Birds;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.support.v7.app.AlertDialog;

import com.example.user.Birds.TestBirds.AddResults;
import com.example.user.Birds.TestBirds.BestLastResult;
import com.example.user.Birds.TestBirds.DBHelper;
import com.example.user.Birds.TestBirds.Database;
import com.example.user.Birds.TestBirds.Figures;
import com.example.user.Birds.TestBirds.Loader;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class FiguresActivity extends AppCompatActivity implements View.OnClickListener,View.OnTouchListener {

    //region Свойства

    Button buttonNo, buttonYes, buttonStartTest, buttonOK;

    ImageView imageViewFigure, smile, answer;

    AlertDialog.Builder builder;

    RelativeLayout relativeLayout;

    TextView textViewTimer, pointsShow;

    AlertDialog.Builder ad;

    Toast toast;

    Timer timer, timer1, timer2, timer3;

    TimerTask timerTask, timerTask1, timerTask2, timerTask3;

    Figures figures = new Figures();

    Loader loader = new Loader();

    BestLastResult bestLastResult = new BestLastResult();

    AddResults addResults = new AddResults();

    Database database = new Database();

    DBHelper db = new DBHelper(this);

    String login, task, commonID, authorization;

    final String URL = "http://psytest.nstu.ru/MobileBirds/addResultFigures.php";

    final String URL1 = "http://psytest.nstu.ru/MobileBirds/AddResultsToCommonTable.php";

    final String URL2 = "http://psytest.nstu.ru/MobileBirds/SendBestLastResultFigures.php";

    int minutes = 1, seconds = 0, bestResult = 0, lastResult = 0;

    boolean wrongAnswer = false;

    Long timeReaction;

    Random random = new Random();

    //endregion

    //region Методы

    //region Загрузка

    public void loadAuth() {
        loader.load(this, "Authorization", "auth", "no");
        authorization = loader.getValue();
    }

    public void loadCurrentLogin() {

        loader.load(this, "LatestLogin", "login", "");
        login = loader.getValue();
    }

    //endregion

    //region Этапы теста

    public void randomFigureAndColor(){
        int r = random.nextInt(4) + 1;

        if(r == 1){
            figures.setFigureCurrent(figures.getFigurePrevious());
            figures.setColorCurrent(figures.getColorPrevious());
        }else{
            figures.randomFigure();
            figures.randomColor();
        }
    }

    public void setInitialParam(){
        figures.randomColor();
        figures.randomFigure();
    }

    public void clickButtonNo(){

        timeReaction = getTimeMilliseconds() - timeReaction;
        showImageAnswer();
        if(!checkFigureAndColor()){
            figures.incrementCorrectAnswers();
            answer.setImageResource(R.drawable.right);

            if(wrongAnswer){
                figures.addCorrectAfterWrongAnswer(timeReaction);
            }else{
                figures.addCorrectAnswer(timeReaction);
            }

            wrongAnswer = false;
        }else{
            answer.setImageResource(R.drawable.error);
            figures.incrementWrongAnswers();
            figures.addWrongAnswer(timeReaction);
            wrongAnswer = true;
        }

        answer.setVisibility(View.VISIBLE);
        Log.d("ans", figures.getCorrectAnswersAmount() + "/" + figures.getWrongAnswersAmount() + "(" + timeReaction + ")");
        timeReaction = getTimeMilliseconds();
    }

    public void clickButtonYes(){

        timeReaction = getTimeMilliseconds() - timeReaction;
        showImageAnswer();
        if(checkFigureAndColor()){
            figures.incrementCorrectAnswers();
            answer.setImageResource(R.drawable.right);

            if(wrongAnswer){
                figures.addCorrectAfterWrongAnswer(timeReaction);
            }else{
                figures.addCorrectAnswer(timeReaction);
            }

            wrongAnswer = false;
        }else{
            answer.setImageResource(R.drawable.error);
            figures.incrementWrongAnswers();
            figures.addWrongAnswer(timeReaction);
            wrongAnswer = true;
        }

        Log.d("ans", figures.getCorrectAnswersAmount() + "/" + figures.getWrongAnswersAmount() + "(" + timeReaction + ")");
        timeReaction = getTimeMilliseconds();
    }

    public void nextStep(){

        figures.setColorPrevious(figures.getColorCurrent());
        figures.setFigurePrevious(figures.getFigureCurrent());
        randomFigureAndColor();
        changeImage();
    }

    public void restart(){
        figures.clearAllArrays();
        figures.setCorrectAnswersAmount(0);
        figures.setWrongAnswersAmount(0);
        figures.setTimeTakeTest(1, 0);
        textViewTimer.setText(figures.getTimeTakeTest().get(figures.KEY_MINUTES) + ":0" + figures.getTimeTakeTest().get(figures.KEY_SECONDS));
        setEnabledButtons();
        setVisibleViews();
        setInitialParam();
        changeImage();
        startTimerFirstFigure();
    }

    //endregion

    //region Таймеры

    public void changeTime(){

        if(!isFinishTest()){
            figures.changeTime();
        }else{
            setDisablesButtons();
            task = "getBestLastResult";
            new RequestTask().execute();
            setInvisibleViews();
            figures.getDeviationWrongAnswers();
            figures.getDeviationCorrectAnswers();
            figures.getDeviationCorrectAfterWrongAnswers();
            addResultToLocalTableResultsTestFigures();
            addResultToLocalTableBestLastResultFigures();
            timer.cancel();
        }
    }

    public void startTimerFirstFigure(){
        if (timer3 != null) {
            timer3.cancel();
        }

        timer3 = new Timer();
        timerTask3 = new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        imageViewFigure.setVisibility(View.INVISIBLE);
                        startTimerShowFigure();
                        startMainTimer();
                    }
                });

            }
        };
        timer3.schedule(timerTask3, 1000);
    }

    public void startMainTimer(){
        if (timer != null) {
            timer.cancel();
        }

        timer = new Timer();
        timerTask = new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        changeTime();
                        if(figures.getTimeTakeTest().get(figures.KEY_SECONDS) >= 10) {
                            textViewTimer.setText(Integer.toString(figures.getTimeTakeTest().get(figures.KEY_MINUTES)) + ":" + Integer.toString(figures.getTimeTakeTest().get(figures.KEY_SECONDS)));
                        }else{
                            textViewTimer.setText(Integer.toString(figures.getTimeTakeTest().get(figures.KEY_MINUTES)) + ":0" + Integer.toString(figures.getTimeTakeTest().get(figures.KEY_SECONDS)));
                        }
                    }
                });

            }
        };
        timer.schedule(timerTask, 1, 1000);
    }

    public void startTimerShowFigure(){
        if (timer1 != null) {
            timer1.cancel();
        }

        timer1 = new Timer();
        timerTask1 = new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        imageViewFigure.setVisibility(View.VISIBLE);
                        nextStep();
                        timeReaction = getTimeMilliseconds();
                    }
                });

            }
        };
        timer1.schedule(timerTask1, 100);
    }

    public void startTimerShowAnswer() {
        if (timer2 != null) {
            timer2.cancel();
        }

        timer2 = new Timer();

        timerTask2 = new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        answer.setVisibility(View.INVISIBLE);
                    }
                });
            }
        };

        timer2.schedule(timerTask2, 200);
    }

    public void cancelTimer(Timer timer){
        if(timer != null){
            timer.cancel();
        }
    }

    public Long getTimeMilliseconds() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "MM/dd/yyyy hh:mm:ss aa", Locale.getDefault());
        Date date = new Date();
        Long milliSeconds = date.getTime();
        Log.d("time", milliSeconds + "");
        return milliSeconds;
    }

    public String getDateTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "MM/dd/yyyy hh:mm:ss aa", Locale.getDefault());
        Date date = new Date();
        Log.d("time", dateFormat.format(date));
        return dateFormat.format(date);
    }

    //endregion

    //region Функции для работы с View элементами

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case 1:
                final String[] levels = {"Уровень 1", "Уровень 2"};
                builder = new android.support.v7.app.AlertDialog.Builder(this);
                builder.setTitle("Выберите уровень"); // заголовок для диалога
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.select_test_dialog, R.id.sp, levels);

                builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int index) {
                        switch (index) {
                            case 0:
                                figures.setLevel("first");
                                break;
                            case 1:
                                figures.setLevel("second");
                                break;
                        }
                    }
                });

                builder.setCancelable(false);
                return builder.create();

            default:
                return null;
        }
    }

    public void setInvisibleViews(){
        buttonNo.setVisibility(View.INVISIBLE);
        buttonYes.setVisibility(View.INVISIBLE);
        textViewTimer.setVisibility(View.INVISIBLE);
        imageViewFigure.setVisibility(View.INVISIBLE);
    }

    public void setVisibleViews(){
        buttonNo.setVisibility(View.VISIBLE);
        buttonYes.setVisibility(View.VISIBLE);
        textViewTimer.setVisibility(View.VISIBLE);
        imageViewFigure.setVisibility(View.VISIBLE);
    }

    public void setDisablesButtons(){
        buttonNo.setEnabled(false);
        buttonYes.setEnabled(false);
    }

    public void setEnabledButtons(){
        buttonNo.setEnabled(true);
        buttonYes.setEnabled(true);
    }

    public void createSmile() {
        smile = new ImageView(this);
        RelativeLayout.LayoutParams par = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        par.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        smile.setVisibility(View.VISIBLE);
        smile.setScaleType(ImageView.ScaleType.FIT_XY);
        par.width = (int) getResources().getDimension(R.dimen.widthArrow);
        par.height = (int) getResources().getDimension(R.dimen.heightArrow);
        relativeLayout.addView(smile, par);
    }

    public void showResults(){

        pointsShow = new TextView(this);

        RelativeLayout.LayoutParams par = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        if (bestResult == 0 && lastResult == 0) {
            pointsShow.setText("Логин: " + login + "\n"
                    + "Текущий результат: " + figures.getCorrectAnswersAmount() + "\n" + "Последний результат: " + "\t\t -" + "\n" + "Лучший результат: " + "\t\t -");
        } else {
            pointsShow.setText("Логин: " + login + "\n"
                    + "Текущий результат: " + figures.getCorrectAnswersAmount() + "\n" + "Последний результат: " + lastResult + "\n" + "Лучший результат: " + bestResult);
        }
        pointsShow.setTextSize((int) getResources().getDimension(R.dimen.textSizeResults));
        pointsShow.setOnClickListener(this);
        pointsShow.setBackgroundColor(Color.parseColor("#AA000000"));
        pointsShow.setTextColor(Color.WHITE);
        pointsShow.setGravity(Gravity.LEFT);
        par.addRule(RelativeLayout.CENTER_HORIZONTAL);
        relativeLayout.addView(pointsShow, par);

        createSmile();
        if (lastResult < figures.getCorrectAnswersAmount()) {
            smile.setImageResource(R.drawable.happy);
        } else {
            smile.setImageResource(R.drawable.sad);
        }
        buttonOK.setVisibility(View.VISIBLE);
    }

    public void showImageAnswer() {
        if (answer == null) {
            answer = new ImageView(this);
            RelativeLayout.LayoutParams par = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            answer.setScaleType(ImageView.ScaleType.FIT_XY);
            answer.setVisibility(View.VISIBLE);
            par.width = (int) getResources().getDimension(R.dimen.widthAnswer);
            par.height = (int) getResources().getDimension(R.dimen.heightAnswer);
            par.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
            relativeLayout.addView(answer, par);
        } else {
            answer.setVisibility(View.VISIBLE);
        }
    }

    public void changeImage(){

        switch (figures.getFigureCurrent()){
            case 1:
                switch(figures.getColorCurrent()){
                    case 1:
                        imageViewFigure.setImageResource(R.drawable.red_square);
                        break;
                    case 2:
                        imageViewFigure.setImageResource(R.drawable.green_square);
                        break;
                    case 3:
                        imageViewFigure.setImageResource(R.drawable.blue_square);
                        break;
                    case 4:
                        imageViewFigure.setImageResource(R.drawable.yellow_square);
                        break;
                }
                break;
            case 2:
                switch(figures.getColorCurrent()){
                    case 1:
                        imageViewFigure.setImageResource(R.drawable.red_triangle);
                        break;
                    case 2:
                        imageViewFigure.setImageResource(R.drawable.green_triangle);
                        break;
                    case 3:
                        imageViewFigure.setImageResource(R.drawable.blue_triangle);
                        break;
                    case 4:
                        imageViewFigure.setImageResource(R.drawable.yellow_triangle);
                        break;
                }
                break;
            case 3:
                switch(figures.getColorCurrent()){
                    case 1:
                        imageViewFigure.setImageResource(R.drawable.red_diamond);
                        break;
                    case 2:
                        imageViewFigure.setImageResource(R.drawable.green_diamond);
                        break;
                    case 3:
                        imageViewFigure.setImageResource(R.drawable.blue_diamond);
                        break;
                    case 4:
                        imageViewFigure.setImageResource(R.drawable.yellow_diamond);
                        break;
                }
                break;
            case 4:
                switch(figures.getColorCurrent()){
                    case 1:
                        imageViewFigure.setImageResource(R.drawable.red_circle);
                        break;
                    case 2:
                        imageViewFigure.setImageResource(R.drawable.green_circle);
                        break;
                    case 3:
                        imageViewFigure.setImageResource(R.drawable.blue_circle);
                        break;
                    case 4:
                        imageViewFigure.setImageResource(R.drawable.yellow_circle);
                        break;
                }
                break;
        }
    }

    public void showToast(String string, int length) {
        if (toast != null) {
            toast.cancel();
        }

        toast = Toast.makeText(this, string, length);
        toast.show();
    }

    public void finishGame() {

        ad = new AlertDialog.Builder(this);
        ad.setCancelable(false);
        ad.setTitle("Тест пройден!");  // заголовок
        ad.setMessage("Хотите пройти тест заново?"); // сообщение
        ad.setPositiveButton("ДА", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                restart();
                if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT)
                    getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
            }
        });
        ad.setNegativeButton("НЕТ", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                Intent intent = new Intent(FiguresActivity.this, MenuActivity.class);
                startActivity(intent);
                finish();
            }
        });
        ad.show();
    }

    //endregion

    //region Проверки

    public boolean isFinishTest(){

        if(figures.getTimeTakeTest().get(figures.KEY_MINUTES) == 0 && figures.getTimeTakeTest().get(figures.KEY_SECONDS) == 0){
            return true;
        }

        return false;
    }

    public boolean checkFigureAndColor(){

        boolean result = true;

        switch (figures.getLevel()){
            case "first":
                if(figures.isSameFigure()){
                    result = true;
                }else{
                    result = false;
                }
                break;

            case "second":
                if(figures.isSameFigure() && figures.isSameColor()){
                    result = true;
                }else{
                    result = false;
                }
                break;
        }

        return  result;
    }

    //endregion

    //region Функции для работы с БД

    public void deleteResult(){
        int id = database.getLastIDFromInfoTest(db);
        if(id != -1) {
            database.deleteFromTable(db, db.TABLE_NAME4, id);
            database.deleteFromTable(db, db.TABLE_NAME2, id);
        }
    }

    public void getBestLastResultFromLocalTable(){
        bestResult = database.getBestResultFromTableFigures(db, login, figures.getLevel());
        lastResult = database.getLastResultFromTableFigures(db, login, figures.getLevel());
    }

    public void addResultToLocalTableInfoTest(){
        database.addToTableInfoTest(db, login, getDateTime(), "Figures", "Online", figures.getLevel());
    }

    public void addResultToLocalTableResultsTestFigures(){
        int id = database.getLastIDFromInfoTest(db);
        database.addToTableResultsTestFigures(db, id, login, figures.getLevel(), (long) figures.getAverageTimeWrongAnswers(),
                (long) figures.getAverageTimeCorrectAnswers(),  (long) figures.getAverageTimeCorrectAfterWrongAnswers(),
                (long) figures.getDeviationWrongAnswers(),  (long) figures.getDeviationCorrectAnswers(),  (long) figures.getDeviationCorrectAfterWrongAnswers());
    }

    public void addResultToLocalTableBestLastResultFigures(){
        database.addToTableBestLastResultsFigures(db, login, figures.getLevel(), figures.getCorrectAnswersAmount() - figures.getWrongAnswersAmount());
    }

    public void prepareDataForAddToCommonTable(){
        addResults.setLogin(login);
        addResults.setURL(URL1);
        addResults.add("login", addResults.getLogin());
        addResults.add("result", Integer.toString(figures.getCorrectAnswersAmount() - figures.getWrongAnswersAmount()));
        addResults.add("version", "mobile version");
        addResults.add("testID", "4102");
        addResults.add("testName", "Тест с фигурами");
        addResults.add("testNameProp", "Figures");
        int id = database.getLastIDFromInfoTest(db);
        String date = database.getDate(db, id);
        addResults.add("date", date);
    }

    public void prepareDataForAddToFiguresResultTable(){
        addResults.setLogin(login);
        addResults.setURL(URL);
        addResults.add("login", addResults.getLogin());
        addResults.add("TrueStim", Integer.toString(figures.getCorrectAnswersAmount()));
        addResults.add("FalseStim", Integer.toString(figures.getWrongAnswersAmount()));
        addResults.add("MidTimeTrue", Integer.toString(figures.getAverageTimeCorrectAnswers()));
        addResults.add("MidTimeFalse", Integer.toString(figures.getAverageTimeWrongAnswers()));
        addResults.add("MidTimeTF", Integer.toString(figures.getAverageTimeCorrectAfterWrongAnswers()));
        addResults.add("StandMissTrue", Integer.toString(figures.getDeviationCorrectAnswers()));
        addResults.add("StandMissFalse", Integer.toString(figures.getDeviationWrongAnswers()));
        addResults.add("StandMissTF", Integer.toString(figures.getDeviationCorrectAfterWrongAnswers()));
        addResults.add("level", figures.getLevel());
    }

    public void prepareToGetBestLastResult(){
        bestLastResult.setLogin(login);
        bestLastResult.setURL(URL2);
        bestLastResult.setKolBirds(figures.getLevel());
    }

    class RequestTask extends AsyncTask<Void, Void, String> {
        @Override
        protected void onPreExecute() {
            switch (task) {
                case "addToCommonTable":
                    prepareDataForAddToCommonTable();
                    break;

                case "addToFigureResults":
                    prepareDataForAddToFiguresResultTable();
                    break;

                case "getBestLastResult":
                    prepareToGetBestLastResult();
                    break;
            }
        }

        @Override
        protected String doInBackground(Void... params) {
            String str = "";
            switch (task) {
                case "addToCommonTable":
                    str = addResults.createRequest();
                    break;

                case "addToFigureResults":
                    str = addResults.createRequest();
                    break;

                case "getBestLastResult":
                    str = bestLastResult.createRequest();
                    break;
            }
            return str;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            switch (task) {
                case "addToCommonTable":
                    if(!result.equals("error")){
                        showToast("Результаты отправлены.", Toast.LENGTH_SHORT);

                        task = "addToFigureResults";
                        new RequestTask().execute();

                    }else{
                        showToast("Проблемы с соединением.", Toast.LENGTH_SHORT);
                    }
                    break;

                case "addToFigureResults":
                    if(!result.equals("error")){
                        showToast("Результаты отправлены.", Toast.LENGTH_SHORT);
                        deleteResult();
                    }else{
                        showToast("Проблемы с соединением.", Toast.LENGTH_SHORT);
                    }
                    break;

                case "getBestLastResult":
                    String string = bestLastResult.getResult(result);
                    if (!string.equals("error") && !string.equals("empty") && !authorization.equals("No")) {
                        bestResult = Integer.parseInt(bestLastResult.getBestResult());
                        lastResult = Integer.parseInt(bestLastResult.getLastResult());
                    } else {
                        getBestLastResultFromLocalTable();
                    }
                    showResults();
                    task = "addToCommonTable";
                    new RequestTask().execute();
                    break;
            }
        }
    }

    //endregion

    //region Переопределнные функции Activity


    @Override
    protected void onResume() {
        super.onResume();
        showDialog(1);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_figures);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        loadCurrentLogin();
        loadAuth();
        buttonNo = (Button) findViewById(R.id.buttonNo);
        buttonYes = (Button) findViewById(R.id.buttonYes);
        buttonOK = (Button) findViewById(R.id.buttonOK);
        buttonOK.setOnClickListener(this);
        buttonOK.setOnTouchListener(this);
        buttonNo.setOnClickListener(this);
        buttonYes.setOnClickListener(this);
        buttonNo.setOnTouchListener(this);
        buttonYes.setOnTouchListener(this);
        buttonStartTest = (Button) findViewById(R.id.buttonStartTest);
        buttonStartTest.setOnClickListener(this);
        buttonStartTest.setOnTouchListener(this);
        relativeLayout = (RelativeLayout) findViewById(R.id.relative_layout);
        imageViewFigure = (ImageView) findViewById(R.id.imageViewFigure);
        textViewTimer = (TextView) findViewById(R.id.textViewTimer);
        figures.setTimeTakeTest(1, 0);
        setInvisibleViews();
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        switch (id) {
            case R.id.buttonNo:
                clickButtonNo();
                startTimerShowAnswer();
                imageViewFigure.setVisibility(View.INVISIBLE);
                startTimerShowFigure();
                break;

            case R.id.buttonYes:
                clickButtonYes();
                startTimerShowAnswer();
                imageViewFigure.setVisibility(View.INVISIBLE);
                startTimerShowFigure();
                break;

            case R.id.buttonStartTest:
                addResultToLocalTableInfoTest();
                buttonStartTest.setVisibility(View.INVISIBLE);
                setVisibleViews();
                setInitialParam();
                changeImage();
                startTimerFirstFigure();
                break;

            case R.id.buttonOK:
                relativeLayout.removeView(smile);
                relativeLayout.removeView(pointsShow);
                buttonOK.setVisibility(View.INVISIBLE);
                finishGame();
                break;
        }
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        int id;
        id = view.getId();
        switch (motionEvent.getAction()) {
            case MotionEvent.ACTION_DOWN:
                switch (id) {
                    case R.id.buttonNo:
                        buttonNo.setAlpha(0.6f);
                        break;
                    case R.id.buttonYes:
                        buttonYes.setAlpha(0.6f);
                        break;
                    case R.id.buttonStartTest:
                        buttonStartTest.setAlpha(0.6f);
                        break;

                }
                break;
            case MotionEvent.ACTION_UP:
                switch (id) {
                    case R.id.buttonNo:
                        buttonNo.setAlpha(1.0f);
                        break;
                    case R.id.buttonYes:
                        buttonYes.setAlpha(1.0f);
                        break;
                    case R.id.buttonStartTest:
                        buttonStartTest.setAlpha(1.0f);
                        break;


                }
                break;
        }
        return false;
    }

    @Override
    public void onBackPressed() {

        cancelTimer(timer);
        cancelTimer(timer1);
        cancelTimer(timer2);
        cancelTimer(timer3);
        deleteResult();

        Intent intent = new Intent(this, MenuActivity.class);
        startActivity(intent);
        finish();
        super.onBackPressed();

    }

    //endregion

    //endregion
}
