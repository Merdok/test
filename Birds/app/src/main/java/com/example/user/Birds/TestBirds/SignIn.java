package com.example.user.Birds.TestBirds;

import android.util.Log;

import org.apache.http.NameValuePair;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by User on 07.05.2016.
 */
public class SignIn extends Request {

    //region Свойства

    private String password;

    //endregion

    //region Методы

    //region Переопределенные методы Request

    @Override
    public String createRequest() {
        try {
            //создаем запрос на сервер
            DefaultHttpClient hc = new DefaultHttpClient();
            ResponseHandler<String> res = new BasicResponseHandler();
            //создаем post запрос
            HttpPost postMethod = new HttpPost(URL);
            //список передаваемых параметров ключ - значение
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);

            //логин
            nameValuePairs.add(new BasicNameValuePair("login", login));
            //пароль
            nameValuePairs.add(new BasicNameValuePair("pass", password));
            //посылаем на сервер
            postMethod.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            //получаем ответ от сервера
            response = hc.execute(postMethod, res);

            return response;

        } catch (Exception e) {
            Log.d("Exp=", e + "");
            return null;
        }
    }

    @Override
    public String getResult(String result) {
        String answer = "";
        try {
            //создаем json объект
            JSONObject json = new JSONObject(result);


            //дальше находим вход в наш json им является ключевое слово data
            JSONArray urls = json.getJSONArray("data");
            //проходим циклом по всем нашим параметрам
            for (int i = 0; i < urls.length(); i++) {
                //читаем что в себе хранит параметр answer
                Log.d("Name", urls.getJSONObject(i).getString("answer").toString());
                answer = urls.getJSONObject(i).getString("answer").toString();
            }
        } catch (JSONException e) {
            Log.e("log_tag", "Error parsing data " + e.toString() + answer);
            e.printStackTrace();
        }
        return answer;
    }

    //endregion

    //region Set-функции

    public void setPassword(String password){
        this.password = password;
    }

    //endregion

    //region Get-функции

    public String getPassword(){
        return this.password;
    }

    //endregion

    //endregion
}
