package com.example.user.Birds.TestBirds;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

public class Figures {

    //region Свойства

    final public  String KEY_SECONDS = "seconds";

    final public String KEY_MINUTES = "minutes";

    //тип текущей фигуры, 1 - квадрат, 2 - треугольник, 3 - ромб, 4 - круг
    private int figureCurrent;

    //тип предыдущей фигуры, 1 - квадрат, 2 - треугольник, 3 - ромб, 4 - круг
    private int figurePrevious = 1;

    //цвет текущей фигуры, 1 - красный, 2 - зеленый, 3 - синий, 4 - желтый
    private int colorCurrent = 1;

    //цвет предыдущей фигуры, 1 - красный, 2 - зеленый, 3 - синий, 4 - желтый
    private int colorPrevious;

    private String level = "first";

    //среднее время неправильных ответов
    private int averageTimeWrongAnswers;

    //среднее время правильных ответов
    private int averageTimeCorrectAnswers;

    //среднее время правильных после неправильных
    private int averageTimeCorrectAfterWrongAnswers;

    //массив времени реакции на неправильные ответы
    private ArrayList<Long> arrayWrongAnswers = new ArrayList<>();

    //массив времени реакции на правильные ответы
    private ArrayList<Long> arrayCorrectAnswers = new ArrayList<>();

    //массив времени реакции на правильные ответы после неправильных
    private ArrayList<Long> arrayCorrectAfterWrongAnswers = new ArrayList<>();

    //количество неправильных ответов
    private int wrongAnswersAmount = 0;

    //количество правильных ответов
    private int correctAnswersAmount = 0;

    //время прохождения теста
    private Map<String, Integer> timeTakeTest = new HashMap<>();

    //рандомайзер
    private Random random = new Random();

    //endregion

    //region Методы

    //region Set-функции

    public void setLevel(String level){
        this.level = level;
    }

    public void setFigureCurrent(int figureCurrent){
        this.figureCurrent = figureCurrent;
    }

    public void setFigurePrevious(int figurePrevious){
        this.figurePrevious = figurePrevious;
    }

    public void setColorPrevious(int colorPrevious){
        this.colorPrevious = colorPrevious;
    }

    public void setColorCurrent(int colorCurrent){
        this.colorCurrent = colorCurrent;
    }

    public void setWrongAnswersAmount(int wrongAnswersAmount){
        this.wrongAnswersAmount = wrongAnswersAmount;
    }

    public void setCorrectAnswersAmount(int correctAnswersAmount){
        this.correctAnswersAmount = correctAnswersAmount;
    }

    public void setTimeTakeTest(int minutes, int seconds){
        this.timeTakeTest.put(KEY_MINUTES, minutes);
        this.timeTakeTest.put(KEY_SECONDS, seconds);
    }

    //endregion

    //region Get-функции

    public String getLevel(){
        return this.level;
    }

    public int getFigureCurrent(){
        return this.figureCurrent;
    }

    public int getFigurePrevious(){
        return this.figurePrevious;
    }

    public int getColorPrevious(){
        return this.colorPrevious;
    }

    public int getColorCurrent(){
        return this.colorCurrent;
    }

    public Map<String, Integer> getTimeTakeTest(){
        return this.timeTakeTest;
    }

    public int getAverageTimeWrongAnswers(){
        return this.averageTimeWrongAnswers;
    }

    public int getAverageTimeCorrectAnswers(){
        return this.averageTimeCorrectAnswers;
    }

    public int getAverageTimeCorrectAfterWrongAnswers(){
        return this.averageTimeCorrectAfterWrongAnswers;
    }

    public int getWrongAnswersAmount(){
        return this.wrongAnswersAmount;
    }

    public int getCorrectAnswersAmount(){
        return this.correctAnswersAmount;
    }

    private void getAverageValueWrongAnswers(){

        float average = 0;

        for(int i = 0; i < arrayWrongAnswers.size(); i++){
            average += arrayWrongAnswers.get(i);
        }

        if(!arrayWrongAnswers.isEmpty()) {
            average /= arrayWrongAnswers.size();
        }

        averageTimeWrongAnswers = (int) average;
    }

    private void getAverageValueCorrectAnswers(){

        float average = 0;

        for(int i = 0; i < arrayCorrectAnswers.size(); i++){
            average += arrayCorrectAnswers.get(i);
        }

        if(!arrayCorrectAnswers.isEmpty()){
            average /= arrayCorrectAnswers.size();
        }

        averageTimeCorrectAnswers = (int) average;
    }

    private void getAverageValueCorrectAfterWrongAnswers(){

        float average = 0;

        for(int i = 0; i < arrayCorrectAfterWrongAnswers.size(); i++){
            average += arrayCorrectAfterWrongAnswers.get(i);
        }

        if(!arrayCorrectAfterWrongAnswers.isEmpty()){
            average /= arrayCorrectAfterWrongAnswers.size();
        }

        averageTimeCorrectAfterWrongAnswers = (int) average;
    }

    public int getDeviationWrongAnswers(){

        double deviation = 0;
        getAverageValueWrongAnswers();

        if(arrayWrongAnswers.size() > 1) {

            for (int i = 0; i < arrayWrongAnswers.size(); i++) {
                deviation += (arrayWrongAnswers.get(i) - averageTimeWrongAnswers) * (arrayWrongAnswers.get(i) - averageTimeWrongAnswers);
            }

            deviation /= (arrayWrongAnswers.size() - 1);
            deviation = Math.sqrt(deviation);
        }

        return (int) deviation;
    }

    public int getDeviationCorrectAnswers(){

        double deviation = 0;
        getAverageValueCorrectAnswers();

        if(arrayCorrectAnswers.size() > 1) {

            for (int i = 0; i < arrayCorrectAnswers.size(); i++) {
                deviation += (arrayCorrectAnswers.get(i) - averageTimeCorrectAnswers) * (arrayCorrectAnswers.get(i) - averageTimeCorrectAnswers);
            }

            deviation /= (arrayCorrectAnswers.size() - 1);
            deviation = Math.sqrt(deviation);
        }

        return (int) deviation;
    }

    public int getDeviationCorrectAfterWrongAnswers(){

        double deviation = 0;
        getAverageValueCorrectAfterWrongAnswers();

        if(arrayCorrectAfterWrongAnswers.size() > 1) {

            for (int i = 0; i < arrayCorrectAfterWrongAnswers.size(); i++) {
                deviation += (arrayCorrectAfterWrongAnswers.get(i) - averageTimeCorrectAfterWrongAnswers) * (arrayCorrectAfterWrongAnswers.get(i) - averageTimeCorrectAfterWrongAnswers);
            }

            deviation /= (arrayCorrectAfterWrongAnswers.size() - 1);
            deviation = Math.sqrt(deviation);
        }

        return (int) deviation;
    }

    //endregion

    //region Add-функции

    public void addWrongAnswer(Long value){
        this.arrayWrongAnswers.add(value);
    }

    public void addCorrectAnswer(Long value){
        this.arrayCorrectAnswers.add(value);
    }

    public void addCorrectAfterWrongAnswer(Long value){
        this.arrayCorrectAfterWrongAnswers.add(value);
    }

    //endregion

    //region random

    public void randomColor(){
        this.colorCurrent = this.random.nextInt(4) + 1;
    }

    public void randomFigure(){
        this.figureCurrent = this.random.nextInt(4) + 1;
    }

    //endregion

    public void changeTime(){
        int seconds = this.timeTakeTest.get(KEY_SECONDS);
        int minutes = this.timeTakeTest.get(KEY_MINUTES);
        seconds--;
        if(seconds < 0){
            seconds = 59;
            minutes--;
            if(minutes < 0){
                minutes = 0;
            }
        }

        setTimeTakeTest(minutes, seconds);
    }

    public boolean isSameColor(){

        if(colorCurrent == colorPrevious){
            return true;
        }else{
            return false;
        }
    }

    public boolean isSameFigure(){

        if(figureCurrent == figurePrevious){
            return true;
        }else{
            return  false;
        }
    }

    public void incrementWrongAnswers(){
        this.wrongAnswersAmount++;
    }

    public void incrementCorrectAnswers(){
        this.correctAnswersAmount++;
    }

    public void clearAllArrays(){
        arrayCorrectAfterWrongAnswers.clear();
        arrayCorrectAnswers.clear();
        arrayWrongAnswers.clear();
    }

    //endregion
}
