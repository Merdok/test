package com.example.user.Birds;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.user.Birds.TestBirds.Checker;
import com.example.user.Birds.TestBirds.DBHelper;
import com.example.user.Birds.TestBirds.Database;
import com.example.user.Birds.TestBirds.GetSettings;
import com.example.user.Birds.TestBirds.Loader;
import com.example.user.Birds.TestBirds.Saver;
import com.example.user.Birds.TestBirds.SignIn;
import com.example.user.Birds.TestBirds.Synchronization;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, View.OnTouchListener, TextWatcher {

    //region Свойства

    Button signIn, signUp;
    EditText loginEditText, passwordEditText;

    RelativeLayout relativeLayout;
    //диалог для сохранения логина и пароля
    AlertDialog.Builder ad;

    Toast toast;

    ProgressBar progressBar;
    //экземплр класса для работы с БД
    Cursor cursor;
    DBHelper db = new DBHelper(this);
    Database database = new Database();

    final String MENU_ACTIVITY = "MenuActivity";
    final String SIGN_UP_ACTIVITY = "SignUpActivity";
    final String URLSignIn = "http://psytest.nstu.ru/MobileBirds/signIn.php";
    final String URLSettings = "http://psytest.nstu.ru/MobileBirds/getSettings.php";

    SignIn classSignIn = new SignIn();
    Loader loader = new Loader();
    Saver saver = new Saver();
    Checker checker = new Checker();
    GetSettings getSettings = new GetSettings();
    Synchronization synchronization = new Synchronization();

    ArrayList<Integer> indexesNoSync = new ArrayList<Integer>();

    String GAME_MODE = "", task, login;

    //endregion

    //region Методы

    //region Загрузки

    public void loadLatestParameters() {
        String logPass[];
        String log = loadLatestLogin();
        logPass = database.getLatestLoginFromTableUsers(db, log);
        if (!logPass[0].equals("No")) {
            loginEditText.setText(logPass[0]);
            passwordEditText.setText(logPass[1]);
        }
    }

    public void loadActivity(String activity) {
        switch (activity) {
            case "MenuActivity":
                Intent intentInstruction = new Intent(MainActivity.this, MenuActivity.class);
                startActivity(intentInstruction);
                finish();
                break;
            case "SignUpActivity":
                Intent intentSignUp = new Intent(MainActivity.this, SignUpActivity.class);
                startActivity(intentSignUp);
                finish();
                break;
        }
    }

    public String loadLatestLogin() {
        loader.load(this, "LatestLogin", "login", "an");
        Log.d("login", loader.getValue());
        return loader.getValue();
    }

    //endregion

    //region Проверки

    public String checkLoginInDB(String login) {
        SQLiteDatabase database = db.getWritableDatabase();
        cursor = database.query(db.TABLE_NAME, null, null, null, null, null, null);
        String log;

        if (cursor.moveToFirst()) {

            do {
                log = cursor.getString(cursor.getColumnIndex(db.KEY_LOGIN));
                if (checker.isStringEquals(login, log)) {
                    return cursor.getString(cursor.getColumnIndex(db.KEY_PASSWORD));
                }

            } while (cursor.moveToNext());
        }
        return "IS";
    }

    public boolean checkLoginPassword() {
        String login = loginEditText.getText().toString().trim();
        String password = passwordEditText.getText().toString().trim();
        if (checker.isEmptyString(login) || checker.isEmptyString(password)) {
            return false;
        }
        return true;
    }

    public void checkAnswerFromServer(String answer) {
        if (checker.isStringEquals(answer, "false")) {
            showToast("Пользователя с таким логином и паролем нет!");
            signIn.setEnabled(true);
        } else {
            saveCurrentLogin(loginEditText.getText().toString().trim());
            saveAuth();
            GAME_MODE = "Online";
            saveGameMode();
            if (checkLoginInDB(loginEditText.getText().toString().trim()).equals("IS")) {
                saveParametres(loginEditText.getText().toString().trim(), passwordEditText.getText().toString().trim());
            } else {
                loadActivity(MENU_ACTIVITY);
                showToast("Вы вошли как " + loginEditText.getText().toString().trim());
            }
            task = "getSettings";
            new RequestTask().execute();
        }
    }

    public boolean checkMagicWord(String string) {
        if (checker.isStringEquals(string, "elbatFoTseb"))
            return true;

        return false;
    }

    //endregion-

    //region Сохранение

    public void saveCurrentLogin(String login) {
        saver.save(this, "LatestLogin", "login", login);
    }

    public void saveParametres(final String login, final String password) {
        ad = new AlertDialog.Builder(this);
        ad.setCancelable(false);
        ad.setTitle("Сохранение данных.");  // заголовок
        ad.setMessage("Запомнить логин и пароль для автоматической авторизации?"); // сообщение
        ad.setPositiveButton("ДА", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                database.addToTableUsers(db, login, password);
                loadActivity(MENU_ACTIVITY);
                showToast("Вы вошли как " + login);
            }
        });
        ad.setNegativeButton("НЕТ", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                loadActivity(MENU_ACTIVITY);
                showToast("Вы вошли как " + login);
            }
        });

        ad.show();

    }

    public void saveAuth() {
        saver.save(this, "Authorization", "auth", "Yes");
    }

    public void saveGameMode() {
        saver.save(this, "GameMode", "Mode", GAME_MODE);
    }

    public void saveSettings() {
        saver.save(this, "Settings", "kol_stim", Integer.toString(getSettings.getKolStim()));
        saver.save(this, "Settings", "kol_dist", Integer.toString(getSettings.getKolDist()));
        saver.save(this, "Settings", "time_max", Integer.toString(getSettings.getTimeMax()));
        saver.save(this, "Settings", "time_min", Integer.toString(getSettings.getTimeMin()));
        saver.save(this, "Settings", "time_step", Integer.toString(getSettings.getTimeStep()));
        saver.save(this, "Settings", "time_between_birds", Integer.toString(getSettings.getTimeBetweenStim()));
    }

    //endregion

    //region Просто функции

    public void showToast(String string) {
        if (toast != null) {
            toast.cancel();
        }

        toast = Toast.makeText(this, string, Toast.LENGTH_SHORT);
        toast.show();


    }

    public void changePasswordEditText() {
        String login = loginEditText.getText().toString().trim(), password = "", string;
        if (checker.isEmptyString(login)) {
            password = "";
        } else {
            string = checkLoginInDB(login);
            if (!checker.isStringEquals(string, "IS")) {
                password = string;
            }
        }
        passwordEditText.setText(password);
    }

    //endregion

    //region Функции для работы с сервером

    class RequestTask extends AsyncTask<String, Void, String> {

        @Override
        protected void onPreExecute() {
            switch (task) {
                case "auth":
                    progressBar.setVisibility(View.VISIBLE);
                    classSignIn.setLogin(loginEditText.getText().toString().trim());
                    classSignIn.setPassword(passwordEditText.getText().toString().trim());
                    signIn.setEnabled(false);
                    break;
                case "getSettings":
                    getSettings.setURL(URLSettings);
                    getSettings.add("login", loginEditText.getText().toString().trim());
                    break;
            }

        }

        @Override
        protected String doInBackground(String... params) {
            String str = "";

            switch (task) {
                case "auth":
                    classSignIn.setURL(params[0]);
                    str = classSignIn.createRequest();
                    break;
                case "getSettings":
                    str = getSettings.createRequest();
                    break;

                case "sync":
                    str = synchronization.synchronize(MainActivity.this, indexesNoSync, login);
                    break;
            }
            return str;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            switch (task) {
                case "auth":
                    progressBar.setVisibility(View.INVISIBLE);
                    if (result != null) {
                        Log.d("result", result);
                        String ans = classSignIn.getResult(result);
                        checkAnswerFromServer(ans);
                    } else {
                        signIn.setEnabled(true);
                        showToast("Проблемы с соединением.");
                    }
                    break;
                case "getSettings":
                    if (!checker.isStringEquals(result, "error")) {
                        getSettings.getResult(result);
                        saveSettings();
                        //login = loadLatestLogin();
                        login = loginEditText.getText().toString().trim();
                        indexesNoSync = database.getNoSyncIndexesFromInfoTest(db, login);
                        if (!indexesNoSync.isEmpty()) {
                            task = "sync";
                            new RequestTask().execute();
                        }
                    }
                    break;

                case "sync":
                    switch (result) {
                        case "empty":
                            showToast("Нет результатов для синхронизации");
                            break;

                        case "error":
                            showToast("Синхронизация не выполнена. Проблемы с соединением");
                            break;

                        case "":
                            showToast("Результаты были синхронизированы");
                            synchronization.update(MainActivity.this, indexesNoSync);
                            indexesNoSync.clear();
                            break;
                    }
                    break;
            }


        }
    }

    //endregion

    //region Переопределенные функции Activity

    @Override
    protected void onDestroy() {
        super.onDestroy();
        System.gc();
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, MenuActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void afterTextChanged(Editable editable) {
        changePasswordEditText();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        signIn = (Button) findViewById(R.id.signIn);
        signUp = (Button) findViewById(R.id.signUp);
        loginEditText = (EditText) findViewById(R.id.loginEditText);
        loginEditText.addTextChangedListener(this);
        passwordEditText = (EditText) findViewById(R.id.passwordEditText);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        loginEditText.setOnClickListener(this);
        passwordEditText.setOnClickListener(this);
        signIn.setOnClickListener(this);
        signIn.setOnTouchListener(this);
        signUp.setOnClickListener(this);
        signUp.setOnTouchListener(this);
        loadLatestParameters();
        loginEditText.setFocusable(false);
        passwordEditText.setFocusable(false);
        progressBar = (ProgressBar) findViewById(R.id.progressBarLoading);
        relativeLayout = (RelativeLayout) findViewById(R.id.relative_layout);
        relativeLayout.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.signIn:

                if(!checkMagicWord(passwordEditText.getText().toString().trim())) {
                    if (checkLoginPassword() == true) {
                        task = "auth";
                        new RequestTask().execute(URLSignIn);
                    } else {
                        signIn.setEnabled(true);
                        showToast("Данные не введены!");
                    }
                }else{
                    Intent intent = new Intent(MainActivity.this, BestResultsTableActivity.class);
                    startActivity(intent);
                }
                break;


            case R.id.signUp:
                signUp.setEnabled(false);
                loadActivity(SIGN_UP_ACTIVITY);
                break;

            case R.id.loginEditText:
                Log.d("Login", "true");
                loginEditText.setFocusableInTouchMode(true);
                passwordEditText.setFocusableInTouchMode(true);
                break;

            case R.id.passwordEditText:
                Log.d("Password", "true");
                loginEditText.setFocusableInTouchMode(true);
                passwordEditText.setFocusableInTouchMode(true);
                break;

        }

    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        int id;
        id = view.getId();
        switch (motionEvent.getAction()) {
            case MotionEvent.ACTION_DOWN:
                switch (id) {
                    case R.id.signIn:
                        signIn.setAlpha(0.6f);
                        break;
                    case R.id.signUp:
                        signUp.setAlpha(0.6f);
                        break;
                }
                break;
            case MotionEvent.ACTION_UP:
                switch (id) {
                    case R.id.signIn:
                        signIn.setAlpha(1.0f);
                        break;
                    case R.id.signUp:
                        signUp.setAlpha(1.0f);
                        break;
                }
                break;
        }
        return false;
    }


    //endregion

    //endregion
}

