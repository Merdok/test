package com.example.user.Birds;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.Birds.TestBirds.AddResults;
import com.example.user.Birds.TestBirds.BestLastResult;
import com.example.user.Birds.TestBirds.DBHelper;
import com.example.user.Birds.TestBirds.Database;
import com.example.user.Birds.TestBirds.Loader;
import com.example.user.Birds.TestBirds.TestBirds;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

public class MainApp extends AppCompatActivity implements View.OnClickListener, View.OnTouchListener {

    //region Свойства

    //region Переменные классов

    TestBirds testBirds = new TestBirds();

    Database database = new Database();

    AddResults addResults = new AddResults();

    DBHelper db = new DBHelper(this);

    Loader loader = new Loader();

    BestLastResult bestLastResult = new BestLastResult();

    //endregion

    //region Промежуточные переменные  для вычислений

    // коэффициент для расчета увеличения количества дистракторов
    int koeffBeg, koeffStep;

    //guessNumber: 1 - угадал цифру, 0 - не угадал  hitBird: 1 - попал в птицу, 0 - не попал
    int hitBird, guessNumber;

    boolean animationGo = false;

    //координаты касания
    float touchX, touchY;

    //endregion

    //region Переменные для AlertDialog
    // диалог с выбором продолжать игру или нет и сохранить результаты или нет

    AlertDialog.Builder ad, adSaveResults;

    //endregion

    //region View-переменные

    //3 картинки(правильный ответ или нет, место где было касание, стрелочка улучшил или ухудшил результат)
    ImageView answer, goal, arrow;

    //textview с параметрами (рандомное число, показ количества очков в конце игры, показ текущего количества птиц)
    TextView targetNumber, pointsShow, kolBirdsToProgressBar;

    //все кнопки
    Button startTest, button1, button2, button3, button4, button5, buttonOK;

    //список птиц, в нашем случае одна птица, но список сделан на случай Параллельного варианта теста
    ArrayList<ImageView> targetImagesBirds = new ArrayList<ImageView>();

    //список дистракторов
    ArrayList<ImageView> imageDistractors = new ArrayList<ImageView>();

    //прогрессбар с количеством птиц и прогрессбар загрузки
    ProgressBar pb, progressBarLoading;

    //endregion

    //region Переменные для экрана
    //для определения параметров экрана
    Display display;
    DisplayMetrics metricsB;
    //endregion

    //region Переменные для БД
    //для работы с БД

    Cursor cursor;

    //endregion

    //region Переменные для таймера

    //Timers
    private Timer timerShowBird, timerHideBird, timerShowAnswer, timerHideStartTest;
    private TimerTask timerTaskShowBird, timerTaskHideBird, timerTaskShowAnswer, timerTaskHideStartTest;

    //endregion

    //region Layout-переменные
    //основной layout
    RelativeLayout relativeLayout;

    //layout со списком кнопок от 1 до 5
    LinearLayout linearLayout;

    //параметры добавления различных View
    RelativeLayout.LayoutParams paramsTargetNumber;
    RelativeLayout.LayoutParams parGoal;
    RelativeLayout.LayoutParams[] paramsImageBird = new RelativeLayout.LayoutParams[10];
    RelativeLayout.LayoutParams[] paramsImageDistractor = new RelativeLayout.LayoutParams[10];
    //endregion

    //region Строковые константы

    final String URL = "http://psytest.nstu.ru/MobileBirds/AddResults.php";

    final String URL1 = "http://psytest.nstu.ru/MobileBirds/AddResultsToCommonTable.php";

    final String URL2 = "http://psytest.nstu.ru/MobileBirds/SendBestLastResult.php";

    //endregion

    //количество попыток при подключению к серверу, максимальное и текущее
    String commonTableID;

    // Offline - без доступа в интернет, Online - с доступом в интернет
    String GAME_MODE = "", authorization, task = "";

    boolean buttonStartTest = false, finish = false, buttonBack = false;

    //endregion

    //region Методы

    //region Загрузки

    public void loadAuth() {
        loader.load(this, "Authorization", "auth", "no");
        authorization = loader.getValue();
    }

    public void loadGameMode() {
        loader.load(this, "GameMode", "Mode", "Offline");
        GAME_MODE = loader.getValue();
    }

    // загрузка парметров теста
    public void loadParameters() {
        SharedPreferences prefs = getSharedPreferences("Settings", MODE_PRIVATE);
        testBirds.setKolBirdsMax(Integer.parseInt(prefs.getString("kol_stim", "10")));
        testBirds.setTimeMax(Integer.parseInt(prefs.getString("time_max", "1000")));
        testBirds.setTimeMin(Integer.parseInt(prefs.getString("time_min", "100")));
        testBirds.setTimeStep(Integer.parseInt(prefs.getString("time_step", "100")));
        testBirds.setKolDistractorsMax(Integer.parseInt(prefs.getString("kol_dist", "3")));
        testBirds.setTimeBetweenShowBird(Integer.parseInt(prefs.getString("time_between_birds", "1000")));
        testBirds.setTimeCurrent(testBirds.getTimeMax());
    }

    //загрузка текущего логина
    public void loadCurrentLogin() {
        String name;
        loader.load(this, "LatestLogin", "login", "");
        name = loader.getValue();
        testBirds.setName(name);
    }

    // запуск функции в отдельном потоке
    public void loadDatabaseTask(String str) {
        DatabaseTask dbTask = new DatabaseTask();
        dbTask.execute(str);
    }

    //endregion

    //region Функции показа результатов

    // диалог с выбором начать тест заново или нет
    public void finishGame() {
        ad = new AlertDialog.Builder(this);
        ad.setCancelable(false);
        ad.setTitle("Тест пройден!");  // заголовок
        ad.setMessage("Хотите пройти тест заново?"); // сообщение
        ad.setPositiveButton("ДА", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                loadDatabaseTask("addToTable2");
                startTest();
                if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT)
                    getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
            }
        });
        ad.setNegativeButton("НЕТ", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                buttonStartTest = false;
                Intent intent = new Intent(MainApp.this, MenuActivity.class);
                startActivity(intent);
                finish();
            }
        });
        ad.show();
    }

    // создание картинки(крестик или галочка) с неправильным или правильным ответом
    public void showImageAnswer() {
        if (answer == null) {
            answer = new ImageView(this);
            RelativeLayout.LayoutParams par = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            answer.setScaleType(ImageView.ScaleType.FIT_XY);
            answer.setVisibility(View.VISIBLE);
            par.width = (int) getResources().getDimension(R.dimen.widthAnswer);
            par.height = (int) getResources().getDimension(R.dimen.heightAnswer);
            par.addRule(RelativeLayout.CENTER_IN_PARENT);
            relativeLayout.addView(answer, par);
        } else {
            answer.setVisibility(View.VISIBLE);
        }
    }

    // создание стрелки, которая показывает улучшение или ухудшение результата
    public void createArrow() {
        arrow = new ImageView(this);
        RelativeLayout.LayoutParams par = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        par.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        arrow.setVisibility(View.VISIBLE);
        arrow.setScaleType(ImageView.ScaleType.FIT_XY);
        par.width = (int) getResources().getDimension(R.dimen.widthArrow);
        par.height = (int) getResources().getDimension(R.dimen.heightArrow);
        relativeLayout.addView(arrow, par);
    }

    public void showResult() {
        //для начала уберем лишнее
        linearLayout.setVisibility(View.INVISIBLE);
        relativeLayout.removeView(goal);
        relativeLayout.removeView(arrow);
        pb.setVisibility(View.INVISIBLE);
        kolBirdsToProgressBar.setVisibility(View.INVISIBLE);
        invisibleImageView();

        task = "getParams";
        loadDatabaseTask("getParams");
        loadDatabaseTask("printTable1");
        loadDatabaseTask("printTable2");
        loadDatabaseTask("addToTable3");
        loadDatabaseTask("printTable3");
    }

    public void alertDialogSaveResults() {

        adSaveResults = new AlertDialog.Builder(this);
        adSaveResults.setCancelable(false);
        adSaveResults.setTitle("Тест пройден!");  // заголовок
        adSaveResults.setMessage("Хотите отправить результаты?"); // сообщение
        adSaveResults.setPositiveButton("ДА", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                loadDatabaseTask("addResultsToServerToCommonTable");
                loadDatabaseTask("addResultsToServerToTableResult");
                finishGame();
            }
        });
        adSaveResults.setNegativeButton("НЕТ", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                finishGame();
            }
        });
        adSaveResults.show();
    }

    //endregion

    //region Функции для таймера

    //таймер для создания (птица, дистракторы, рандомное число от 1 до 5)
    public void startTimer() {

        if (timerShowBird != null) {
            timerShowBird.cancel();
        }

        timerShowBird = new Timer();
        timerTaskShowBird = new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {

                        createTextViewTargetNumber();
                        createImageViewBird();
                        createImagesDistractors();
                    }
                });

            }
        };
        timerShowBird.schedule(timerTaskShowBird, testBirds.getTimeBetweenShowBird());
    }

    // таймер скрытия (птица, дистракторыб число от 1 до 5)
    public void startTimer1() {
        if (timerHideBird != null) {
            timerHideBird.cancel();
        }

        timerHideBird = new Timer();

        timerTaskHideBird = new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        invisibleImageView();
                        invisibleTextView();
                        invisibleImagesDistractors();
                        relativeLayout.setEnabled(true);
                        testBirds.updateKolBirdsCurrent();
                        updateProgressBar();
                        animationGo = true;
                    }
                });
            }
        };
        timerHideBird.schedule(timerTaskHideBird, testBirds.getTimeBetweenShowBird() + testBirds.getTimeCurrent());
    }

    //таймер для показа правильного или неправильного ответа
    public void startTimer2() {
        if (timerShowAnswer != null) {
            timerShowAnswer.cancel();
        }

        timerShowAnswer = new Timer();

        timerTaskShowAnswer = new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        answer.setVisibility(View.INVISIBLE);
                    }
                });
            }
        };

        timerShowAnswer.schedule(timerTaskShowAnswer, 200);
    }

    //таймер для скрытия кнопки startTest
    public void startTimer3() {
        if (timerHideStartTest != null) {
            timerHideStartTest.cancel();
        }

        timerHideStartTest = new Timer();
        timerTaskHideStartTest = new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        startTest.performClick();
                    }
                });

            }
        };
        timerHideStartTest.schedule(timerTaskHideStartTest, 200);
    }

    //endregion

    //region Функции, вызывающиеся во время выполнения теста

    // изменение параметров после каждого этапа теста
    public void changeParamOfTest() {

        testBirds.updateBackground();
        testBirds.updateTimeCurrent();

        if (testBirds.getKolBirdsCurrent() > koeffBeg) {
            testBirds.setTimeCurrent();
            testBirds.updateKolDistractorsCurrent();
            koeffBeg += koeffStep;
        }

    }

    //устновление необходимых параметров для запуска следующего этапа теста
    public void newStep() {

        if (testBirds.getKolBirdsCurrent() >= testBirds.getKolBirdsMax()) {
            finish = true;
            showResult();
        } else {
            linearLayout.setVisibility(View.INVISIBLE);
            relativeLayout.setEnabled(false);
            deleteImageViewFromRelativeLayout();
            deleteImageViewFromArrayList();
            deleteImagesDistractorsFromRelativeLayout();
            deleteImagesDistractorsFromArrayList();
            changeParamOfTest();
            updateProgressBar();
            setBackgroundImage(testBirds.getBackground());
            startTimer();
            startTimer1();
            relativeLayout.removeView(goal);
            testBirds.getDistractorsCoord().clear();
        }
    }

    // генерация рандомного числа от 1 до 5
    public void setTargetNumber() {
        testBirds.randomTargetNumber();
    }

    //обновление прогрессбара с количество птиц
    public void updateProgressBar() {
        pb.setProgress(testBirds.getKolBirdsCurrent());
        kolBirdsToProgressBar.setText(testBirds.getKolBirdsCurrent() + "/" + testBirds.getKolBirdsMax());
    }

    // создание картинки цели по координатам нажатия
    public void showGoal(int x, int y) {
        parGoal = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        goal = new ImageView(this);
        goal.setVisibility(View.VISIBLE);
        goal.setImageResource(R.drawable.goal);
        goal.setScaleType(ImageView.ScaleType.FIT_XY);
        parGoal.width = (int) getResources().getDimension(R.dimen.widthGoal);
        parGoal.height = (int) getResources().getDimension(R.dimen.heightGoal);
        // отнимаем 64 из-за паддингов Relative-layout
        parGoal.leftMargin = x - 64;
        parGoal.topMargin = y - 64;
        relativeLayout.addView(goal, parGoal);
    }

    // запускает анимацию мигания цели
    public void startAnimation(ImageView imageView) {
        if (animationGo == true) {
            Animation animation = AnimationUtils.loadAnimation(this, R.anim.target);
            //animation.setAnimationListener(this);
            imageView.startAnimation(animation);
            animationGo = false;
        }
    }

    // останавливает анимацию мигания цели
    public void clearAnimation(ImageView imageView) {
        imageView.clearAnimation();
    }

    // устанавливает картинку на задний фон
    public void setBackgroundImage(int num) {

        switch (num) {
            case 1:
                relativeLayout.setBackgroundResource(R.drawable.winter);
                break;
            case 2:
                relativeLayout.setBackgroundResource(R.drawable.winter1);
                break;
            case 3:
                relativeLayout.setBackgroundResource(R.drawable.winter2);
                break;
            case 4:
                relativeLayout.setBackgroundResource(R.drawable.winter3);
                break;
        }
    }

    //endregion

    //region Функции создания и инициализации

    // иницилизация параметров для отправки результатов на сервер
    public void initializeParametresForAddResults(String tableName) {
        if (tableName.equals("CommonTable")) {
            addResults.setLogin(testBirds.getName());
            addResults.setURL(URL1);
            addResults.add("login", addResults.getLogin());

            int id1 = database.getIDFromTableInfoTest(db, testBirds.getName());
            final int points = database.getSumPoints(db, id1, testBirds.getName());

            addResults.add("result", Integer.toString(points));
            addResults.add("version", "mobile version");
            addResults.add("testID", "9614");
            addResults.add("testName", "Тест с птицами");
            addResults.add("testNameProp", "Birds");
            int id = database.getLastIDFromInfoTest(db);
            String date = database.getDate(db, id);
            Log.d("date", date + " toServer");
            addResults.add("date", date);
        } else {
            DBHelper db = new DBHelper(this);
            int id = this.database.getLastIDFromInfoTest(db);
            SQLiteDatabase database = db.getWritableDatabase();
            Cursor cursor;
            int i = 0;
            cursor = database.query(db.TABLE_NAME1, null, db.KEY_LOGIN + " =? AND " + db.KEY_ID + " =?",
                    new String[]{testBirds.getName(), Integer.toString(id)}, null, null, null);
            addResults.setURL(URL);
            addResults.setKolBirdsMax(testBirds.getKolBirdsMax());
            addResults.add("kolBirds", Integer.toString(addResults.getKolBirdsMax()));
            addResults.add("id", commonTableID);
            if (cursor.moveToFirst()) {
                do {
                    addResults.setLogin(testBirds.getName());
                    addResults.setHit(cursor.getInt(cursor.getColumnIndex(db.KEY_HIT)));
                    addResults.setDistractors(cursor.getInt(cursor.getColumnIndex(db.KEY_DISTRACTORS)));
                    addResults.setDistance(cursor.getInt(cursor.getColumnIndex(db.KEY_DIST)));
                    addResults.setPoints(cursor.getInt(cursor.getColumnIndex(db.KEY_POINTS)));
                    addResults.setHitBird(cursor.getInt(cursor.getColumnIndex(db.KEY_HITBIRD)));
                    addResults.add("login" + Integer.toString(i), addResults.getLogin());
                    addResults.add("hit" + Integer.toString(i), Integer.toString(addResults.getHit()));
                    addResults.add("distractors" + Integer.toString(i), Integer.toString(addResults.getDistractors()));
                    addResults.add("distance" + Integer.toString(i), Integer.toString(addResults.getDistance()));
                    addResults.add("points" + Integer.toString(i), Integer.toString(addResults.getPoints()));
                    addResults.add("hitBird" + Integer.toString(i), Integer.toString(addResults.getHitBird()));
                    i++;
                } while (cursor.moveToNext());
            }

            cursor.close();
            db.close();
        }
    }

    public void initialParamForGetBestLastResult() {
        bestLastResult.setLogin(testBirds.getName());
        bestLastResult.setURL(URL2);
        bestLastResult.setKolBirds(Integer.toString(testBirds.getKolBirdsMax()));
    }

    // устанавливает начальне значения для запуска нового теста
    public void startTest() {
        testBirds.setTimeCurrent(testBirds.getTimeMax());
        testBirds.setBackground(1);
        testBirds.setKolBirdsCurrent(0);
        testBirds.setRightAnswers(0);
        testBirds.setKolHits(0);
        testBirds.setPoints(0);
        relativeLayout.setEnabled(false);
        pb.setVisibility(View.VISIBLE);
        kolBirdsToProgressBar.setVisibility(View.VISIBLE);
        deleteImageViewFromRelativeLayout();
        deleteImageViewFromArrayList();
        koeffStep = testBirds.getKolBirdsMax() / (testBirds.getKolDistractorsMax() + 1);
        koeffBeg = koeffStep;
        deleteImagesDistractorsFromRelativeLayout();
        deleteImagesDistractorsFromArrayList();
        testBirds.setKolDistractorsCurrent(0);
        updateProgressBar();
        startTimer();
        startTimer1();
    }

    // создание стимула-птицы
    public void createImageViewBird() {
        ImageView image;

        for (int i = 0; i < 1; i++) {
            image = new ImageView(this);
            image.setImageResource(R.drawable.bird);
            image.setScaleType(ImageView.ScaleType.FIT_XY);
            image.setEnabled(false);
            paramsImageBird[i] = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            paramsImageBird[i].width = (int) getResources().getDimension(R.dimen.widthBird);
            paramsImageBird[i].height = (int) getResources().getDimension(R.dimen.heightBird);
            testBirds.randCoordinateBird(paramsImageBird[i].width, paramsImageBird[i].height);
            paramsImageBird[i].leftMargin = testBirds.getBirdPosX();
            paramsImageBird[i].topMargin = testBirds.getBirdPosY();
            image.setOnTouchListener(this);
            image.setId(i);
            targetImagesBirds.add(image);
            relativeLayout.addView(image, paramsImageBird[i]);

        }

    }

    // создание дистрактора
    public void createImagesDistractors() {
        ImageView image;

        for (int i = 0; i < testBirds.getKolDistractorsCurrent(); i++) {
            image = new ImageView(this);
            image.setImageResource(R.drawable.distractor);
            image.setScaleType(ImageView.ScaleType.FIT_XY);
            image.setEnabled(false);
            paramsImageDistractor[i] = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            paramsImageDistractor[i].width = (int) getResources().getDimension(R.dimen.widthDistractor);
            paramsImageDistractor[i].height = (int) getResources().getDimension(R.dimen.heightDistractor);
            testBirds.randCoordinateDistractor(paramsImageDistractor[i].width, paramsImageDistractor[i].height);
            paramsImageDistractor[i].leftMargin = testBirds.getDistractorPosX();
            paramsImageDistractor[i].topMargin = testBirds.getDistractorPosY();
            image.setOnTouchListener(this);
            image.setId(i);
            imageDistractors.add(image);
            relativeLayout.addView(image, paramsImageDistractor[i]);

        }
    }

    public void setInitial() {

        relativeLayout.removeView(startTest);
        pb.setVisibility(View.VISIBLE);
        pb.setMax(testBirds.getKolBirdsMax());
    }

    // инициализация всех View-элементов
    public void initViews() {
        startTest = (Button) findViewById(R.id.startTest);
        buttonOK = (Button) findViewById(R.id.buttonOK);
        button1 = (Button) findViewById(R.id.button1);
        button2 = (Button) findViewById(R.id.button2);
        button3 = (Button) findViewById(R.id.button3);
        button4 = (Button) findViewById(R.id.button4);
        button5 = (Button) findViewById(R.id.button5);
        kolBirdsToProgressBar = (TextView) findViewById(R.id.pointsTextView);
        button1.setOnClickListener(this);
        pb = (ProgressBar) findViewById(R.id.progressBar);
        progressBarLoading = (ProgressBar) findViewById(R.id.progressBarLoading);
        startTest.setOnClickListener(this);
        startTest.setOnTouchListener(this);
        buttonOK.setOnClickListener(this);
        buttonOK.setOnTouchListener(this);
        button2.setOnClickListener(this);
        button3.setOnClickListener(this);
        button4.setOnClickListener(this);
        button5.setOnClickListener(this);
        relativeLayout = (RelativeLayout) findViewById(R.id.relative_layout);
        relativeLayout.setEnabled(false);
        linearLayout = (LinearLayout) findViewById(R.id.linearLayout);
        linearLayout.setVisibility(View.INVISIBLE);
        pb.setVisibility(View.INVISIBLE);
        paramsTargetNumber = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        relativeLayout.setOnTouchListener(this);
    }

    // создание TextView с рандомным число от 1 до 5
    public void createTextViewTargetNumber() {
        if (targetNumber == null) {
            targetNumber = new TextView(this);
            setTargetNumber();
            targetNumber.setText(Integer.toString(testBirds.getTargetNumber()));
            targetNumber.setTextSize((int) getResources().getDimension(R.dimen.textSizeTargetNumber));
            targetNumber.setTextColor(Color.BLACK);
            targetNumber.setBackgroundColor(Color.parseColor("#00000000"));
            paramsTargetNumber.addRule(RelativeLayout.CENTER_IN_PARENT);
            relativeLayout.addView(targetNumber, paramsTargetNumber);
        } else {
            targetNumber.setVisibility(View.VISIBLE);
            setTargetNumber();
            targetNumber.setText(Integer.toString(testBirds.getTargetNumber()));
        }
    }

    //endregion

    //region Функции удаления и скрытия

    // удаление результатов из таблиц кроме лучшего и последнего
    public void deleteFromTable(String str) {

        int id;
        switch (str) {
            case "Last":
                id = database.getLastIDFromInfoTest(db);
                database.deleteFromTable(db, db.TABLE_NAME1, id);
                database.deleteFromTable(db, db.TABLE_NAME3, id);
                database.deleteFromTable(db, db.TABLE_NAME2, id);
                Log.d("Delete Database", "Data has been deleted emergency");
                break;
        }


    }

    public void deleteResult(){
        int id2 = database.getLastIDFromInfoTest(db);
        database.deleteFromTable(db, db.TABLE_NAME1, id2);
        database.deleteFromTable(db, db.TABLE_NAME2, id2);
    }

    // скрытие стимула-птицы
    public void invisibleImageView() {

        for (int i = 0; i < 1; i++) {
            targetImagesBirds.get(i).setVisibility(View.INVISIBLE);
        }
    }

    // скрытие дистрактора
    public void invisibleImagesDistractors() {
        for (int i = 0; i < testBirds.getKolDistractorsCurrent(); i++) {
            imageDistractors.get(i).setVisibility(View.INVISIBLE);
        }
    }

    // скртыие рандомного числа от 1 до 5
    public void invisibleTextView() {
        targetNumber.setVisibility(View.INVISIBLE);
    }

    // удаление стимула птицы с экрана
    public void deleteImageViewFromRelativeLayout() {
        for (int i = 0; i < 1; i++) {
            relativeLayout.removeView(targetImagesBirds.get(i));
        }
    }

    // удаление дистракторов  с экрана
    public void deleteImagesDistractorsFromRelativeLayout() {
        for (int i = 0; i < testBirds.getKolDistractorsCurrent(); i++) {
            relativeLayout.removeView(imageDistractors.get(i));
        }
    }

    // удаление дистракторов из списка
    public void deleteImagesDistractorsFromArrayList() {
        imageDistractors.clear();
    }

    // удаление стимул-птиц из списка
    public void deleteImageViewFromArrayList() {
        targetImagesBirds.clear();
    }
    //endregion

    //region Проверки

    public void checkClickBird(boolean clickBird, int guessNumber) {
        if (clickBird) {
            hitBird = 1;
            testBirds.calcPoints(testBirds.getTimeCurrent(), hitBird, guessNumber);
            testBirds.updateKolHits();
        } else {
            hitBird = 0;
            testBirds.calcPoints(testBirds.getTimeCurrent(), hitBird, guessNumber);
        }
    }

    public void checkSelectedNumber(int randomNumber, int selectedNumber) {
        boolean clickBird = testBirds.checkClickBird(touchX, touchY, targetImagesBirds.get(0).getWidth(), targetImagesBirds.get(0).getHeight());

        if (randomNumber == selectedNumber) {
            guessNumber = 1;
            answer.setImageResource(R.drawable.right);
            checkClickBird(clickBird, guessNumber);
            testBirds.updateRightAnswers();
        } else {
            guessNumber = 0;
            answer.setImageResource(R.drawable.error);
            checkClickBird(clickBird, guessNumber);
        }
    }

    //endregion

    //region Просто функции

    // получение текущей даты
    public String getDateTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "MM/dd/yyyy hh:mm:ss aa", Locale.getDefault());
        Date date = new Date();
        Log.d("time", dateFormat.format(date));
        Long milliSeconds = date.getTime();
        Log.d("time2", milliSeconds.toString());
        return dateFormat.format(date);
        //return milliSeconds.toString();
    }

    // получение параметров экрана
    public void getDisplaySize() {
        display = getWindowManager().getDefaultDisplay();
        metricsB = new DisplayMetrics();
        display.getMetrics(metricsB);
        Log.d("dim", metricsB.widthPixels + ", " + metricsB.heightPixels);
        testBirds.setDisplayWidth(metricsB.widthPixels);
        testBirds.setDisplayHeight((int) (metricsB.heightPixels * 0.75));
    }

    //endregion

    //region Класс для работы с БД и отправки результатов на сервер в отдельном потоке

    public void sendResultsToServer() {
        if (authorization.equals("Yes")) {
            loadDatabaseTask("addResultsToServerToCommonTable");
            loadDatabaseTask("addResultsToServerToTableResult");
        }
    }

    class DatabaseTask extends AsyncTask<String, Void, Integer> {

        private int bestResult;

        private int latestResult;

        private int id;

        private int pointsRes;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressBarLoading.setVisibility(View.VISIBLE);
            Log.d("DatabaseAsyncTask", "Begin");
        }

        @Override
        protected Integer doInBackground(String... voids) {

            try {
                String str = voids[0];
                switch (str) {
                    case "addToTable1":
                        id = database.getIDFromTableInfoTest(db, testBirds.getName());
                        database.addToTableResultsTestBirds(db, id, testBirds.getName(), guessNumber, hitBird, testBirds.getKolDistractorsCurrent(), testBirds.getDistance(), testBirds.getPoints(), GAME_MODE);
                        Log.d("doInBackground", "Data has been added to table1");
                        break;
                    case "addToTable2":
                        database.addToTableInfoTest(db, testBirds.getName(), getDateTime(), "Birds", GAME_MODE, Integer.toString(testBirds.getKolBirdsMax()));
                        Log.d("doInBackground", "Data has been added to table2");
                        break;
                    case "addToTable3":
                        int id1 = database.getIDFromTableInfoTest(db, testBirds.getName());
                        final int points = database.getSumPoints(db, id1, testBirds.getName());
                        database.addToTableBestLastResultsBirds(db, testBirds.getName(), testBirds.getKolBirdsMax(), points);
                        Log.d("doInBackground", "Data has been added to table3");
                        break;
                    case "deleteFromTables":
                        deleteFromTable("First");
                        break;

                    case "deleteFromTablesEmergency":
                        deleteFromTable("Last");
                        break;

                    case "printTable1":
                        TimeUnit.SECONDS.sleep(0);
                        database.printTableResultsTest(db);
                        Log.d("doInBackground", "Table1 has been printed");
                        break;
                    case "printTable2":
                       // database.printTableInfoTest(db);
                        Log.d("doInBackground", "Table2 has been printed");
                        break;
                    case "printTable3":
                        //database.printTableResultsTestForUser(db);
                        Log.d("doInBackground", "Table3 has been printed");
                        break;

                    case "getParams":
                        String string;
                        initialParamForGetBestLastResult();
                        string = bestLastResult.createRequest();
                        string = bestLastResult.getResult(string);
                        if (!string.equals("error") && !string.equals("empty") && !authorization.equals("No")) {
                            this.bestResult = Integer.parseInt(bestLastResult.getBestResult());
                            this.latestResult = Integer.parseInt(bestLastResult.getLastResult());
                        } else {
                            this.bestResult = database.getBestResultFromTableBirds(db, testBirds.getName(), testBirds.getKolBirdsMax());
                            this.latestResult = database.getLastResultFromTableBirds(db, testBirds.getName(), testBirds.getKolBirdsMax());
                        }

                        id = database.getIDFromTableInfoTest(db, testBirds.getName());
                        pointsRes = database.getSumPoints(db, id, testBirds.getName());
                        Log.d("doInBackground", "Params have been got" + "," + id + "," + pointsRes);
                        return -1;

                    case "addResultsToServerToTableResult":
                        initializeParametresForAddResults("TableResults");
                        String ans = addResults.createRequest();
                        Log.d("result", ans);
                        if (!ans.equals("error")) {
                            return 0;
                        }
                        return 1;

                    case "addResultsToServerToCommonTable":
                        initializeParametresForAddResults("CommonTable");
                        String ans1 = addResults.createRequest();
                        Log.d("result", ans1);
                        if (!ans1.equals("error")) {
                            commonTableID = ans1;
                            return 2;
                        }
                        return 1;
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            return -2;
        }

        @Override
        protected void onPostExecute(Integer result) {
            super.onPostExecute(result);
            progressBarLoading.setVisibility(View.INVISIBLE);

            Log.d("DatabaseAsyncTask", "End " + result);
            switch (result) {
                case -1:
                    createTextViewWithResults();
                    sendResultsToServer();
                    buttonOK.setVisibility(View.VISIBLE);
                    break;
                case 0:
                    Toast.makeText(MainApp.this, "Результаты отправлены", Toast.LENGTH_SHORT).show();
                    deleteResult();
                    break;
                case 1:
                    Toast.makeText(MainApp.this, "Проблемы с соединением.", Toast.LENGTH_SHORT).show();
                    int id = database.getLastIDFromInfoTest(db);
                    database.updateRecord(db, id, "NoSync");
                    break;
                case 2:
                    Toast.makeText(MainApp.this, "Результаты отправлены", Toast.LENGTH_SHORT).show();
                    break;
            }

        }

        public void createTextViewWithResults() {

            pointsShow = new TextView(MainApp.this);
            String outName;
            if (testBirds.getName().equals("an")) {
                outName = "anonymous";
            } else {
                outName = testBirds.getName();
            }
            RelativeLayout.LayoutParams par = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            if (bestResult == 0 && latestResult == 0) {
                pointsShow.setText("Логин: " + outName + "\n" + "Правильные ответы: " + testBirds.getRightAnswers()
                        + " (" + testBirds.getRightAnswers() * 100 / testBirds.getKolBirdsMax() + "%)" + "\n"
                        + "Количество попаданий: " + testBirds.getKolHits() + " (" + testBirds.getKolHits() * 100 / testBirds.getKolBirdsMax() + "%)" + "\n"
                        + "Текущий результат: " + pointsRes + "\n" + "Последний результат: " + "\t\t -" + "\n" + "Лучший результат: " + "\t\t -");
            } else {
                pointsShow.setText("Логин: " + outName + "\n" + "Правильные ответы: " + testBirds.getRightAnswers()
                        + " (" + testBirds.getRightAnswers() * 100 / testBirds.getKolBirdsMax() + "%)" + "\n"
                        + "Количество попаданий: " + testBirds.getKolHits() + " (" + testBirds.getKolHits() * 100 / testBirds.getKolBirdsMax() + "%)" + "\n"
                        + "Текущий результат: " + pointsRes + "\n" + "Последний результат: " + latestResult + "\n" + "Лучший результат: " + bestResult);
            }
            pointsShow.setTextSize((int) getResources().getDimension(R.dimen.textSizeResults));
            pointsShow.setOnClickListener(MainApp.this);
            pointsShow.setBackgroundColor(Color.parseColor("#AA000000"));
            pointsShow.setTextColor(Color.WHITE);
            pointsShow.setGravity(Gravity.LEFT);
            par.addRule(RelativeLayout.CENTER_HORIZONTAL);
            relativeLayout.addView(pointsShow, par);

            createArrow();
            if (latestResult < pointsRes) {
                arrow.setImageResource(R.drawable.happy);
            } else {
                arrow.setImageResource(R.drawable.sad);
            }

            Log.d("Dimension", goal.getHeight() + "," + goal.getWidth());
        }
    }

    //endregion

    //region Переопределенные функции Activity

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_MENU) {
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
                loadDatabaseTask("deleteFromTablesEmergency");
                finish();
            }
            event.startTracking();

            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        System.gc();
    }

    @Override
    protected void onResume() {
        super.onResume();
        //убрать кнопки
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT)
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);


        Log.d("Resume", "activity was resumed " + buttonStartTest + "," + finish);
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("Stop", "activity was stopped");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("Pause", "activity was paused");
        if (buttonStartTest && !finish && !buttonBack) {
            loadDatabaseTask("deleteFromTablesEmergency");
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        setContentView(R.layout.activity_main_app);
        initViews();
        getDisplaySize();
        loadParameters();
        loadCurrentLogin();
        loadAuth();
        loadGameMode();
        showImageAnswer();
        koeffStep = testBirds.getKolBirdsMax() / (testBirds.getKolDistractorsMax() + 1);
        koeffBeg = koeffStep;
        getDateTime();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.startTest:
                getDisplaySize();
                setInitial();
                setBackgroundImage(testBirds.getBackground());
                startTimer();
                startTimer1();
                break;

            case R.id.button1:
                checkSelectedNumber(testBirds.getTargetNumber(), 1);
                loadDatabaseTask("addToTable1");
                clearAnimation(goal);
                newStep();
                showImageAnswer();
                startTimer2();

                Log.d("dimension", targetNumber.getWidth() + "," + targetNumber.getHeight());
                break;
            case R.id.button2:
                checkSelectedNumber(testBirds.getTargetNumber(), 2);
                loadDatabaseTask("addToTable1");
                clearAnimation(goal);
                newStep();
                showImageAnswer();
                startTimer2();

                break;
            case R.id.button3:
                checkSelectedNumber(testBirds.getTargetNumber(), 3);
                loadDatabaseTask("addToTable1");
                clearAnimation(goal);
                newStep();
                showImageAnswer();
                startTimer2();

                break;
            case R.id.button4:
                checkSelectedNumber(testBirds.getTargetNumber(), 4);
                loadDatabaseTask("addToTable1");
                clearAnimation(goal);
                newStep();
                showImageAnswer();
                startTimer2();

                break;
            case R.id.button5:
                checkSelectedNumber(testBirds.getTargetNumber(), 5);
                loadDatabaseTask("addToTable1");
                clearAnimation(goal);
                newStep();
                showImageAnswer();
                startTimer2();
                break;

            case R.id.buttonOK:
                relativeLayout.removeView(arrow);
                relativeLayout.removeView(pointsShow);
                linearLayout.setVisibility(View.INVISIBLE);
                relativeLayout.setEnabled(false);
                invisibleImageView();
                invisibleImagesDistractors();
                buttonOK.setVisibility(View.INVISIBLE);
                finishGame();
                break;
        }
        animationGo = false;
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        touchX = motionEvent.getX();
        touchY = motionEvent.getY();
        int id;
        id = view.getId();

        switch (motionEvent.getAction()) {
            case MotionEvent.ACTION_DOWN:
                if (id == R.id.startTest) {
                    startTest.setAlpha(0.6f);
                } else {
                    if (id == R.id.relative_layout) {
                        showGoal((int) touchX, (int) touchY);
                        startAnimation(goal);
                        relativeLayout.setEnabled(false);
                        linearLayout.setVisibility(View.VISIBLE);
                        targetImagesBirds.get(0).setVisibility(View.VISIBLE);
                    }
                }
                break;
            case MotionEvent.ACTION_UP:
                if (id == R.id.startTest) {
                    startTest.setAlpha(1.0f);
                    loadDatabaseTask("addToTable2");
                    buttonStartTest = true;
                    startTimer3();
                }
                break;
        }

        return false;
    }

    @Override
    public void onBackPressed() {
        if (buttonStartTest && !finish) {
            loadDatabaseTask("deleteFromTablesEmergency");
        }
        buttonBack = true;
        Intent intent = new Intent(this, MenuActivity.class);
        startActivity(intent);
        finish();
        super.onBackPressed();

    }

    //endregion

    //endregion
}
