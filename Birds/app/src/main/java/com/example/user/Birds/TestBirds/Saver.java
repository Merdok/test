package com.example.user.Birds.TestBirds;

import android.content.Context;
import android.content.SharedPreferences;


public class Saver {

    public void save(Context context, String fileName, String key, String value){
        SharedPreferences prefs = context.getSharedPreferences(fileName, Context.MODE_PRIVATE);
        SharedPreferences.Editor ed = prefs.edit();
        ed.putString(key, value);
        ed.commit();
    }
}
