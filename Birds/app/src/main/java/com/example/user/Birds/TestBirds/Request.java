package com.example.user.Birds.TestBirds;

/**
 * Created by User on 07.05.2016.
 */
abstract public class Request {

    //region Свойства

    protected String URL;

    protected String response;

    protected String login;

    //endregion

    //region Методы

    //region Абстрактные функции

    public abstract String createRequest();

    public abstract String getResult(String result);

    //endregion

    //region Set-функции

    public void setURL(String URL){
        this.URL = URL;
    }

    public void setLogin(String login){
        this.login = login;
    }

    public void setResponse(String response){
        this.response = response;
    }

    //endregion

    //region Get-функции

    public String getLogin(){
        return this.login;
    }

    public String getURL(){
        return this.URL;
    }

    public String getResponse(){
        return this.response;
    }

    //endregion

    //endregion
}
