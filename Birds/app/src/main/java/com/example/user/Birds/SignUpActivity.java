package com.example.user.Birds;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.Birds.TestBirds.Checker;
import com.example.user.Birds.TestBirds.DBHelper;
import com.example.user.Birds.TestBirds.Database;
import com.example.user.Birds.TestBirds.Saver;
import com.example.user.Birds.TestBirds.SignUp;


public class SignUpActivity extends AppCompatActivity implements View.OnClickListener, View.OnTouchListener {

    //region Свойства

    // кнопка зарегистрироваться
    Button buttonOK;
    //диалог сохранения пароля и логина
    AlertDialog.Builder ad;
    //прогрессбар во время коннекта к серверу
    ProgressBar progressBar;
    //главный layout
    RelativeLayout relativeLayout;
    //не используется, надо убрать
    TextView textView;
    //всплывающее окно с информацией
    Toast toast;
    //поля куда надо вводить данные
    EditText editTextName, editTextSurname, editTextEmail, editTextPassword, editTextAge, editTextLogin;
    //строковые константы
    final String URL = "http://psytest.nstu.ru/MobileBirds/signUp.php";
    final String GMAIL = "gmail.com";
    final String MAIL = "mail.ru";
    //класс с функциями для работы с БД
    Database database = new Database();
    //класс с функциями создания БД и таблиц
    DBHelper db = new DBHelper(this);

    SignUp signUp = new SignUp();
    Saver saver = new Saver();
    Checker checker = new Checker();

    int ATTEMPT_MAX_TO_CONNECT = 15, attemptCurrent = 0;

    String GAME_MODE = "";

    //endregion

    //region Методы

    //region Сохранение

    // сохранение логина
    public void saveLogin() {
        saver.save(this, "LatestLogin", "login", editTextLogin.getText().toString().trim());
    }

    // запоминание логина и пароля, чтобы не вводить 100 раз
    public void saveParametres(final String login, final String password) {
        ad = new AlertDialog.Builder(this);
        ad.setCancelable(false);
        ad.setTitle("Сохранение данных!");  // заголовок
        ad.setMessage("Запомнить логин и пароль для автоматической авторизации?"); // сообщение
        ad.setPositiveButton("ДА", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                database.addToTableUsers(db, login, password);
                showToast("Вы вошли как " + editTextLogin.getText().toString().trim());
                loadActivity();
            }
        });
        ad.setNegativeButton("НЕТ", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                showToast("Вы вошли как " + login);
                loadActivity();
            }
        });

        ad.show();

    }

    public void saveAuth(){
        saver.save(this, "Authorization", "auth", "Yes");
    }

    public void saveGameMode() {
        saver.save(this, "GameMode", "Mode", GAME_MODE);
    }

    //endregion

    //region Просто функции

    // показ всплывающего окна с сообщением
    public void showToast(String message) {
        if (toast != null) {
            toast.cancel();
        }

        toast = Toast.makeText(this, message, Toast.LENGTH_SHORT);
        toast.show();


    }

    // удаление из строки, пришедшей с сервера, не нужных символов
    public String clearUpAnswer(String answer) {
        String str1 = new String("&quot;"), str2 = new String("<br>");
        answer = answer.replaceAll(str1, "");
        answer = answer.replaceAll(str2, "");
        return answer;
    }

    //endregion

    //region Проверки

    // главный обработчик ввода данных
    public boolean mainLocalHandler() {
        // проверка на пустоту
        if (isEmptyString(editTextName.getText().toString().trim()) || isEmptyString(editTextSurname.getText().toString().trim())
                || isEmptyString(editTextEmail.getText().toString().trim()) || isEmptyString(editTextPassword.getText().toString().trim())
                || isEmptyString(editTextAge.getText().toString().trim())) {
            showToast("Не все поля заполнены");
            return false;
        }
        // проверка на недопустимые символы в поле, где необходимо ввести возраст
        else {
            if (isFirstZero(editTextAge.getText().toString().trim()) || isDotInStr(editTextAge.getText().toString().trim())) {
                showToast("Ошибка ввода данных");
                return false;
            }
            // проверка на длину пароля
            else {
                if (checkLengthPassword(editTextPassword.getText().toString().trim()) == false) {
                    showToast("Пароль должен содержать не менее 6 символов");
                    return false;
                }
                // проверка на правильность ввода e-mail
                else {
                    if (checkEmail(editTextEmail.getText().toString().trim()) == false) {
                        showToast("Некорректный e-mail");
                        return false;
                    }
                }
            }
        }
        return true;
    }

    // проверка на пустоту строки
    public boolean isEmptyString(String string) {
        return checker.isEmptyString(string);
    }

    // проверка на первый символ в строке, чтобы он не был 0
    public boolean isFirstZero(String string) {
        return checker.isFirstZero(string);
    }

    // проверка наличия точки в строке
    public boolean isDotInStr(String string) {
        return checker.isDotInString(string);
    }

    // проверка длины пароля
    public boolean checkLengthPassword(String string) {
        return checker.isLengthStringMore(string, 6);
    }

    // проверка корректности e-mail
    public boolean checkEmail(String string) {
        return checker.isAtInString(string);
    }

    // проверка ответа с сервера
    public void checkAnswerFromServer(String answer) {
        if (checker.isStringEquals(answer, "")) {
            showToast("Регистрация прошла успешно!");
            saveLogin();
            saveAuth();
            GAME_MODE = "Online";
            saveGameMode();
            saveParametres(editTextLogin.getText().toString().trim(), editTextPassword.getText().toString().trim());
        } else {
            showToast(answer);
        }
    }

    //endregion

    //region Загрузка и инициализация

    // инициализация View-элементов
    public void initial() {
        buttonOK = (Button) findViewById(R.id.buttonOK);
        buttonOK.setOnClickListener(this);
        buttonOK.setOnTouchListener(this);
        editTextName = (EditText) findViewById(R.id.editTextName);
        editTextSurname = (EditText) findViewById(R.id.editTextSurname);
        editTextEmail = (EditText) findViewById(R.id.editTextEmail);
        editTextPassword = (EditText) findViewById(R.id.editTextPassword);
        editTextAge = (EditText) findViewById(R.id.editTextAge);
        editTextLogin = (EditText) findViewById(R.id.editTextLogin);
        relativeLayout = (RelativeLayout) findViewById(R.id.relative_layout);
        progressBar = (ProgressBar) findViewById(R.id.progressBarLoading);
    }

    public void loadActivity() {
        Intent intent = new Intent(this, MenuActivity.class);
        startActivity(intent);
        finish();
    }

    //endregion

    //region Функции для работы с сервером

    class RequestTask extends AsyncTask<String, Void, String> {
        @Override
        protected void onPreExecute() {
            progressBar.setVisibility(View.VISIBLE);
            signUp.setLogin(editTextLogin.getText().toString().trim());
            signUp.setPassword(editTextPassword.getText().toString().trim());
            signUp.setName(editTextName.getText().toString().trim());
            signUp.setSurname(editTextSurname.getText().toString().trim());
            signUp.setEmail(editTextEmail.getText().toString().trim());
            attemptCurrent = 0;
        }

        @Override
        protected String doInBackground(String... params) {
            String str = "";
            while (attemptCurrent < ATTEMPT_MAX_TO_CONNECT) {
                signUp.setURL(params[0]);
                str = signUp.createRequest();
                if (str != null) {
                    break;
                }
                //reconnectWifi();
                attemptCurrent++;
            }

            return str;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            progressBar.setVisibility(View.INVISIBLE);
            if (result != null) {
                result = clearUpAnswer(result);
                String ans = signUp.getResult(result);
                checkAnswerFromServer(ans);
            } else {
                showToast("Проблемы с соединением.");
            }
        }
    }

    //endregion

    //region Переопределенные функции Activity

    @Override
    protected void onDestroy() {
        super.onDestroy();
        System.gc();
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
        super.onBackPressed();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        editTextLogin = (EditText) findViewById(R.id.editTextLogin);
        editTextLogin.setFocusableInTouchMode(true);
        initial();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.buttonOK:
                if (mainLocalHandler()) {
                    new RequestTask().execute(URL);
                }
                break;
        }
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        int id;
        id = view.getId();

        switch (motionEvent.getAction()) {
            case MotionEvent.ACTION_DOWN:
                switch (id) {
                    case R.id.buttonOK:
                        buttonOK.setAlpha(0.6f);
                        break;
                }
                break;
            case MotionEvent.ACTION_UP:
                switch (id) {
                    case R.id.buttonOK:
                        buttonOK.setAlpha(1.0f);
                        break;
                }
                break;
        }
        return false;
    }

    //endregion

    //endregion
}
