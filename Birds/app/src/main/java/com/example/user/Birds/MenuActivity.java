package com.example.user.Birds;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.user.Birds.TestBirds.Checker;
import com.example.user.Birds.TestBirds.DBHelper;
import com.example.user.Birds.TestBirds.Database;
import com.example.user.Birds.TestBirds.GetSettings;
import com.example.user.Birds.TestBirds.Loader;
import com.example.user.Birds.TestBirds.Saver;
import com.example.user.Birds.TestBirds.SignIn;
import com.example.user.Birds.TestBirds.Synchronization;

import java.util.ArrayList;

public class MenuActivity extends AppCompatActivity implements View.OnClickListener, View.OnTouchListener {

    //region Свойства

    Loader loader = new Loader();
    Saver saver = new Saver();
    Checker checker = new Checker();
    SignIn signIn = new SignIn();
    GetSettings getSettings = new GetSettings();
    Database database = new Database();
    DBHelper db = new DBHelper(this);
    Synchronization synchronization = new Synchronization();

    Button buttonExit, buttonTest, buttonInstruction, buttonEnter, buttonSettings, buttonEnterMainMenu;
    ProgressBar progressBar;

    String GAME_MODE = "";
    AlertDialog.Builder builder;

    int points = 0;

    Toast toast;

    final String URLSignIn = "http://psytest.nstu.ru/MobileBirds/signIn.php";

    final String URLSettings = "http://psytest.nstu.ru/MobileBirds/getSettings.php";

    String login, password;

    ArrayList<Integer> indexesNoSync = new ArrayList<Integer>();

    String authorization = "No", runMenu, task;

    boolean buttonHomePressed;
    //endregion

    //region Методы

    public void hideAppButtons() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT)
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
    }

    public void setButtonsDisabled() {
        buttonEnter.setEnabled(false);
        buttonExit.setEnabled(false);
        buttonInstruction.setEnabled(false);
        buttonSettings.setEnabled(false);
        buttonTest.setEnabled(false);
        buttonEnterMainMenu.setEnabled(false);
    }

    public void setButtonsEnabled() {
        buttonEnter.setEnabled(true);
        buttonExit.setEnabled(true);
        buttonInstruction.setEnabled(true);
        buttonSettings.setEnabled(true);
        buttonTest.setEnabled(true);
        buttonEnterMainMenu.setEnabled(true);
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case 1:
                final String[] testName = {"Птицы", "Фигуры"};
                builder = new AlertDialog.Builder(this);
                builder.setTitle("Выберите тест"); // заголовок для диалога
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.select_test_dialog, R.id.sp, testName);

                builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int index) {
                        switch (index) {
                            case 0:
                                loadTestBirdsActivity();
                                break;
                            case 1:
                                loadTestFiguresActivity();
                                break;
                        }
                    }
                });

                return builder.create();

            default:
                return null;
        }
    }

    public void loadTestBirdsActivity() {
        if (!login.equals("an")) {
            buttonHomePressed = false;
            runMenu = "second";
            saveAuth();
            Intent intent = new Intent(this, MainApp.class);
            startActivity(intent);
            finish();
        } else {
            showToast("Необходимо создать аккаунт!", Toast.LENGTH_SHORT);
        }
    }

    public void loadTestFiguresActivity() {
        runMenu = "second";
        saveAuth();
        Intent intent = new Intent(MenuActivity.this, FiguresActivity.class);
        startActivity(intent);
        finish();
    }

    //region Show-функции

    View.OnClickListener viewClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Button button;
            button = (Button) v.findViewById(v.getId());
            if (button.getText().toString().equals("Войти")) {
                runMenu = "second";
                saveAuth();
                Intent intent = new Intent(MenuActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            } else {
                showPopupMenu(v);
            }
            buttonHomePressed = false;
        }
    };


    private void showPopupMenu(View v) {
        PopupMenu popupMenu = new PopupMenu(this, v);
        popupMenu.inflate(R.menu.popupmenu);

        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {

            @Override
            public boolean onMenuItemClick(MenuItem item) {

                switch (item.getItemId()) {

                    case R.id.PersonalArea:
                        //showToast("Личного кабинета еще нет!", Toast.LENGTH_SHORT);
                        runMenu = "second";
                        saveAuth();
                        Intent intent = new Intent(MenuActivity.this, PersonalAreaActivity.class);
                        startActivity(intent);
                        return true;

                    case R.id.Logout:
                        authorization = "No";
                        runMenu = "second";
                        saveAuth();
                        GAME_MODE = "Offline";
                        saveGameMode();
                        buttonEnter.setText("Войти");
                        Intent intent2 = new Intent(MenuActivity.this, MainActivity.class);
                        startActivity(intent2);
                        finish();
                        return true;

                    default:
                        return false;
                }
            }
        });

        popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {

            @Override
            public void onDismiss(PopupMenu menu) {
                hideAppButtons();
            }
        });

        popupMenu.show();
    }

    public void showToast(String string, int length) {
        if (toast != null) {
            toast.cancel();
        }

        toast = Toast.makeText(this, string, length);
        toast.show();


    }

    //endregion

    //region Загрузки

    public void loadCurrentLogin() {
        loader.load(this, "LatestLogin", "login", "an");
        login = loader.getValue();
        Log.d("login", login);
    }

    public boolean loadPassword() {
        String logPass[];
        logPass = database.getLatestLoginFromTableUsers(db, login);
        if (!logPass[0].equals("No")) {
            password = logPass[1];
            return true;
        }
        return false;
    }

    public void loadAuth() {
        loader.load(this, "Authorization", "auth", "No");
        authorization = loader.getValue();
        loader.load(this, "Authorization", "menu", "first");
        runMenu = loader.getValue();
    }

    //endregion

    //region Сохранение

    public void saveGameMode() {
        saver.save(this, "GameMode", "Mode", GAME_MODE);
    }

    public void saveCurrentLogin() {
        saver.save(this, "LatestLogin", "login", login);
    }

    public void saveAuth() {
        saver.save(this, "Authorization", "auth", authorization);
        saver.save(this, "Authorization", "menu", runMenu);
    }

    public void saveSettings() {
        saver.save(this, "Settings", "kol_stim", Integer.toString(getSettings.getKolStim()));
        saver.save(this, "Settings", "kol_dist", Integer.toString(getSettings.getKolDist()));
        saver.save(this, "Settings", "time_max", Integer.toString(getSettings.getTimeMax()));
        saver.save(this, "Settings", "time_min", Integer.toString(getSettings.getTimeMin()));
        saver.save(this, "Settings", "time_step", Integer.toString(getSettings.getTimeStep()));
        saver.save(this, "Settings", "time_between_birds", Integer.toString(getSettings.getTimeBetweenStim()));
    }

    //endregion

    //region Проверки

    public boolean checkLogin() {
        return checker.isStringEquals(login, "an");
    }

    public boolean checkAuth() {
        return checker.isStringEquals(authorization, "Yes");
    }

    public boolean checkMenu() {
        return checker.isStringEquals(runMenu, "first");
    }

    public void checkAnswerFromServer(String answer) {
        if (checker.isStringEquals(answer, "false")) {
            showToast("Пользователя с таким логином и паролем нет!", Toast.LENGTH_SHORT);
            authorization = "No";
            saveAuth();
            GAME_MODE = "Offline";
            saveGameMode();
            buttonEnter.setText("Войти");
        } else {
            authorization = "Yes";
            saveAuth();
            GAME_MODE = "Online";
            saveGameMode();
            showToast("Авторизация выполнена. Вы вошли как " + login, Toast.LENGTH_SHORT);
            setButtonEnterTitle();

            task = "getSettings";
            new RequestTask().execute();
        }
    }

    //endregion

    //region Авторизация

    public void authorization() {
        if (!checkLogin()) {
            if (autoAuthorization() && checkAuth() && checkMenu()) {
                task = "auth";
                new RequestTask().execute();
            }
        } else {
            if (checkMenu()) {
                showToast("У вас нет аккаунта! Нажмите ВОЙТИ в левом верхнем углу", Toast.LENGTH_LONG);
            }
        }
    }

    public boolean autoAuthorization() {
        return loadPassword();
    }

    public void setButtonEnterTitle() {
        if (!checkAuth()) {
            buttonEnter.setText("Войти");
            buttonEnterMainMenu.setText("Войти");
        } else {
            buttonEnter.setText("Логин: " + login);
            buttonEnterMainMenu.setText("Логин: " + login);
        }
    }

    //endregion

    //region Класс для отправки данных на сервер в отдельном потоке

    class RequestTask extends AsyncTask<Void, Void, String> {
        @Override
        protected void onPreExecute() {
            switch (task) {
                case "auth":
                    buttonEnter.setText("Войти");
                    buttonEnterMainMenu.setText("Войти");
                    progressBar.setVisibility(View.VISIBLE);
                    setButtonsDisabled();
                    signIn.setLogin(login);
                    signIn.setPassword(password);
                    signIn.setURL(URLSignIn);
                    showToast("Идет авторизация...", Toast.LENGTH_SHORT);
                    break;

                case "sync":
                    showToast("Началась синхронизация...", Toast.LENGTH_SHORT);
                    break;

                case "getSettings":
                    getSettings.setURL(URLSettings);
                    getSettings.add("login", login);
                    break;
            }

        }

        @Override
        protected String doInBackground(Void... params) {
            String str = "";
            switch (task) {
                case "auth":
                    str = signIn.createRequest();
                    break;
                case "sync":
                    Log.d("tut", "i'm here");
                    str = synchronization.synchronize(MenuActivity.this, indexesNoSync, login);
                    Log.d("res", str);
                    break;

                case "getSettings":
                    str = getSettings.createRequest();
                    Log.d("Page", str);
                    break;
            }
            return str;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            switch (task) {
                case "auth":
                    progressBar.setVisibility(View.INVISIBLE);
                    setButtonsEnabled();
                    if (result != null) {
                        Log.d("result", result);
                        String ans = signIn.getResult(result);
                        checkAnswerFromServer(ans);
                    } else {
                        authorization = "No";
                        saveAuth();
                        GAME_MODE = "Offline";
                        saveGameMode();
                        buttonEnter.setText("Войти");
                        showToast("Авторизация не выполнена. Проблемы с соединением.", Toast.LENGTH_LONG);
                    }
                    break;

                case "sync":
                    switch (result) {
                        case "empty":
                            showToast("Нет результатов для синхронизации", Toast.LENGTH_SHORT);
                            break;

                        case "error":
                            showToast("Синхронизация не выполнена. Проблемы с соединением", Toast.LENGTH_LONG);
                            break;

                        case "":
                            showToast("Результаты были синхронизированы", Toast.LENGTH_SHORT);
                            synchronization.update(MenuActivity.this, indexesNoSync);
                            indexesNoSync.clear();
                            break;
                    }
                    break;

                case "getSettings":
                    if (!checker.isStringEquals(result, "error")) {
                        getSettings.getResult(result);
                        saveSettings();
                        if (!indexesNoSync.isEmpty()) {
                            task = "sync";
                            new RequestTask().execute();
                        }
                    }
                    break;
            }
        }
    }

    //endregion

    //region Переопределенные функции Activity

    @Override
    protected void onDestroy() {
        super.onDestroy();
        System.gc();
        Log.d("destroyMenu", "yes");
    }

    @Override
    public boolean onKeyUp(int keyCode, KeyEvent event) {

        switch (keyCode) {
            case KeyEvent.KEYCODE_MENU:
                showToast("Menu", Toast.LENGTH_SHORT);
                break;
        }

        return super.onKeyUp(keyCode, event);
    }

    @Override
    protected void onStop() {
        super.onStop();
        buttonHomePressed = false;
        Log.d("stopMenu", "yes");
    }

    @Override
    protected void onResume() {
        super.onResume();
        //убрать кнопки
        hideAppButtons();
        Log.d("resumeMenu", "yes");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        Log.d("createMenu", "yes");
        buttonHomePressed = true;
        loadCurrentLogin();
        loadAuth();
        indexesNoSync = database.getNoSyncIndexesFromInfoTest(db, login);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        buttonEnterMainMenu = (Button) findViewById(R.id.buttonEnterMainMenu);
        buttonExit = (Button) findViewById(R.id.buttonExit);
        buttonTest = (Button) findViewById(R.id.buttonTest);
        buttonInstruction = (Button) findViewById(R.id.buttonInstruction);
        buttonEnter = (Button) findViewById(R.id.buttonEnter);
        buttonSettings = (Button) findViewById(R.id.buttonSettings);
        progressBar = (ProgressBar) findViewById(R.id.progressBarLoading);
        //createSelectTestDialog();
        buttonEnterMainMenu.setOnClickListener(viewClickListener);
        buttonEnterMainMenu.setOnTouchListener(this);
        buttonSettings.setOnClickListener(this);
        buttonSettings.setOnTouchListener(this);
        buttonEnter.setOnClickListener(viewClickListener);
        buttonEnter.setOnTouchListener(this);
        buttonExit.setOnClickListener(this);
        buttonExit.setOnTouchListener(this);
        buttonTest.setOnClickListener(this);
        buttonTest.setOnTouchListener(this);
        buttonInstruction.setOnClickListener(this);
        buttonInstruction.setOnTouchListener(this);
        setButtonEnterTitle();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        buttonHomePressed = false;
        runMenu = "first";
        saveAuth();
    }

    @Override
    protected void onStart() {
        super.onStart();
        authorization();
        Log.d("startMenu", "yes");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("pauseMenu", "yes");
        if (buttonHomePressed) {
            //showToast("Home pressed",Toast.LENGTH_SHORT);
        }
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        switch (id) {
            case R.id.buttonTest:
                showDialog(1);
                break;

            case R.id.buttonExit:
                buttonHomePressed = false;
                runMenu = "first";
                saveAuth();
                finish();
                break;

            case R.id.buttonSettings:
                if (!login.equals("an")) {
                    buttonHomePressed = false;
                    runMenu = "second";
                    saveAuth();
                    Intent intent1 = new Intent(this, SettingsActivity.class);
                    startActivity(intent1);
                } else {
                    showToast("Для просмотра настроек необходимо создать аккаунт!", Toast.LENGTH_SHORT);
                }
                break;

            case R.id.buttonInstruction:
                buttonHomePressed = false;
                runMenu = "second";
                saveAuth();
                Intent intent2 = new Intent(this, InstructionActivity.class);
                startActivity(intent2);
                finish();
                break;

        }
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        int id;
        id = view.getId();
        switch (motionEvent.getAction()) {
            case MotionEvent.ACTION_DOWN:
                switch (id) {
                    case R.id.buttonTest:
                        buttonTest.setAlpha(0.6f);
                        break;
                    case R.id.buttonExit:
                        buttonExit.setAlpha(0.6f);
                        break;
                    case R.id.buttonInstruction:
                        buttonInstruction.setAlpha(0.6f);
                        break;
                    case R.id.buttonEnter:
                        buttonEnter.setBackgroundColor(getResources().getColor(R.color.buttonEnterDown));
                        break;
                    case R.id.buttonSettings:
                        buttonSettings.setBackgroundColor(getResources().getColor(R.color.buttonEnterDown));
                        break;
                    case R.id.buttonEnterMainMenu:
                        buttonEnterMainMenu.setAlpha(0.6f);
                        break;
                }
                break;
            case MotionEvent.ACTION_UP:
                switch (id) {
                    case R.id.buttonTest:
                        buttonTest.setAlpha(1.0f);
                        break;
                    case R.id.buttonExit:
                        buttonExit.setAlpha(1.0f);
                        break;
                    case R.id.buttonInstruction:
                        buttonInstruction.setAlpha(1.0f);
                        break;
                    case R.id.buttonEnter:
                        buttonEnter.setBackgroundColor((int) getResources().getColor(R.color.buttonEnterUp));
                        break;
                    case R.id.buttonSettings:
                        buttonSettings.setBackgroundColor((int) getResources().getColor(R.color.buttonEnterUp));
                        break;
                    case R.id.buttonEnterMainMenu:
                        buttonEnterMainMenu.setAlpha(1.0f);
                        break;
                }
                break;
        }
        return false;
    }

    //endregion

    //endregion
}
