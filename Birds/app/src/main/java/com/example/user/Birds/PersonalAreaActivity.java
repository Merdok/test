package com.example.user.Birds;

import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.example.user.Birds.TestBirds.BestLastResult;
import com.example.user.Birds.TestBirds.DBHelper;
import com.example.user.Birds.TestBirds.Database;
import com.example.user.Birds.TestBirds.GetKolBirds;
import com.example.user.Birds.TestBirds.Loader;

import java.util.ArrayList;

public class PersonalAreaActivity extends AppCompatActivity implements View.OnClickListener, View.OnTouchListener {

    //region Свойства

    TextView textViewLogin, textViewBest, textViewLatest, textViewTestName, textView;

    ProgressBar progressBar;

    ArrayAdapter<String> adapterBirds, adapterFigures;

    Spinner spinner;

    String login, kolStimuls, task, test = "Птицы";

    boolean flag = false, clickTestName = false;

    final String URL = "http://psytest.nstu.ru/MobileBirds/SendBestLastResult.php";
    final String URL1 = "http://psytest.nstu.ru/MobileBirds/sendKolBirds.php";

    String[] kolBirds, levels = {"Уровень 1(сравнение по фигуре)", "Уровень 2(сравнение по цвету и фигуре)"};

    Button buttonBack;

    Database database = new Database();
    Loader loader = new Loader();
    DBHelper db = new DBHelper(this);
    BestLastResult bestLastResult = new BestLastResult();
    GetKolBirds getKolBirds = new GetKolBirds();

    //endregion

    //region Методы

    public void loadKolBirds() {
        loader.load(this, "Settings", "kol_stim", "10");
        kolStimuls = loader.getValue();
    }

    public void hideAppButtons() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT)
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
    }

    public void getKolBirds() {
        ArrayList<Integer> kolBirds;
        kolBirds = database.getKolBirds(db, login);
        if (kolBirds.get(0) != 0) {
            this.kolBirds = new String[kolBirds.size()];
            for (int i = 0; i < kolBirds.size(); i++) {
                this.kolBirds[i] = Integer.toString(kolBirds.get(i));
            }
        } else {
            this.kolBirds = new String[1];
            this.kolBirds[0] = "-";
        }
    }

    public void fillFieldsWithoutInternet() {

        Spinner spinner = (Spinner) findViewById(R.id.spinner);
        getKolBirds();
        createAdapter();

        if (!kolBirds[0].equals("-")) {
            kolStimuls = spinner.getSelectedItem().toString();
            setBestResult(Integer.parseInt(spinner.getSelectedItem().toString()));
            setLatestResult(Integer.parseInt(spinner.getSelectedItem().toString()));

        } else {
            Toast.makeText(PersonalAreaActivity.this, "Результатов нет, так как вы ни разу не проходили тест", Toast.LENGTH_SHORT).show();
            spinner.setEnabled(false);
            textViewBest.setText("Лучший результат: -");
            textViewLatest.setText("Последний результат: -");
        }
    }

    public int getPosition(String string, Spinner spinner) {
        ArrayAdapter adapter = (ArrayAdapter) spinner.getAdapter();
        int position = adapter.getPosition(string);
        return position;
    }

    public void createAdapter() {

        if(!clickTestName){
            // адаптер
            adapterBirds = new ArrayAdapter<String>(this, R.layout.row, R.id.sp, kolBirds);
            spinner.setAdapter(adapterBirds);
            textViewTestName.setText("Тест: " + "Птицы");
            textView.setText("Кол-во стимулов:");
            //adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        }else{
            // адаптер
            adapterFigures = new ArrayAdapter<String>(this, R.layout.row, R.id.sp, levels);
            spinner.setAdapter(adapterFigures);
            textViewTestName.setText("Тест: " + "Фигуры");
            textView.setText("Уровень:");
            //adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        }

    }

    public int getSize(String string) {
        int size = 0;
        for (int i = 0; i < string.length(); i++) {
            if (string.charAt(i) == '(') {
                size++;
            }
        }

        return size;
    }

    public void parserKolBirdsFromServer(String kolBirds) {
        int index = 0;
        this.kolBirds = new String[getSize(kolBirds)];
        for (int i = 0; i < kolBirds.length(); i++) {
            if (kolBirds.charAt(i) == '(') {
                int j = i + 1;
                String str = "";
                while (kolBirds.charAt(j) != ')') {
                    str += Character.toString(kolBirds.charAt(j));
                    j++;
                }
                Log.d("kol", str);
                this.kolBirds[index] = str;
                index++;
            }
        }
    }

    public void initViews() {
        buttonBack = (Button) findViewById(R.id.buttonBack);
        textViewLogin = (TextView) findViewById(R.id.textViewLogin);
        textViewBest = (TextView) findViewById(R.id.textViewBest);
        textViewLatest = (TextView) findViewById(R.id.textViewLatest);
        textViewTestName = (TextView) findViewById(R.id.textViewTestName);
        textView = (TextView) findViewById(R.id.textView);

        textViewTestName.setOnClickListener(this);
        buttonBack.setOnClickListener(this);
        buttonBack.setOnTouchListener(this);
    }

    public void loadCurrentLogin() {
        loader.load(this, "LatestLogin", "login", "an");
        login = loader.getValue();
    }

    public void setLogin() {
        textViewLogin.setText("Логин: " + login);
    }

    public void setBestResult(int kol) {
        int res = database.getBestResultFromTableBirds(db, login, kol);
        textViewBest.setText("Лучший результат: " + res);
    }

    public void setLatestResult(int kol) {
        int res = database.getLastResultFromTableBirds(db, login, kol);
        textViewLatest.setText("Последний результат: " + res);
    }

    class RequestTask extends AsyncTask<Void, Void, String> {
        @Override
        protected void onPreExecute() {

            switch (task) {
                case "kolBirds":
                    getKolBirds.setLogin(login);
                    getKolBirds.setURL(URL1);
                    break;

                case "bestLastResult":
                    progressBar.setVisibility(View.VISIBLE);
                    bestLastResult.setURL(URL);
                    bestLastResult.setLogin(login);
                    bestLastResult.setKolBirds(spinner.getSelectedItem().toString());
                    break;
            }


        }

        @Override
        protected String doInBackground(Void... params) {
            String str = "";

            switch (task) {
                case "kolBirds":
                    str = getKolBirds.createRequest();
                    break;

                case "bestLastResult":
                    str = bestLastResult.createRequest();
                    break;
            }

            return str;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            switch (task) {
                case "kolBirds":
                    Log.d("res", result);
                    result = getKolBirds.getResult(result);
                    switch (result) {
                        case "error":
                            flag = true;
                            Toast.makeText(PersonalAreaActivity.this, "Проблемы с загрузкой результатов с сервера. Данные были взяты с этого устройства.", Toast.LENGTH_SHORT).show();
                            fillFieldsWithoutInternet();
                            break;
                        case "empty":
                            Toast.makeText(PersonalAreaActivity.this, "Результатов нет, так как вы ни разу не проходили тест", Toast.LENGTH_SHORT).show();
                            textViewBest.setText("Лучший результат: -");
                            textViewLatest.setText("Последний результат: -");
                            break;
                        default:
                            parserKolBirdsFromServer(result);
                            createAdapter();
                            //Log.d("log", "I'm here2222");
                            if(!flag) {
                                task = "bestLastResult";
                                new RequestTask().execute();
                            }
                            break;
                    }

                    break;

                case "bestLastResult":
                    progressBar.setVisibility(View.INVISIBLE);
                    Log.d("res", result);
                    switch (result) {
                        case "error":
                            Toast.makeText(PersonalAreaActivity.this, "Проблемы с загрузкой результатов с сервера. Данные были взяты с этого устройства.", Toast.LENGTH_SHORT).show();
                            fillFieldsWithoutInternet();
                            break;
                        case "empty":
                            Toast.makeText(PersonalAreaActivity.this, "Результатов нет, так как вы ни разу не проходили тест", Toast.LENGTH_SHORT).show();
                            spinner.setEnabled(false);
                            textViewBest.setText("Лучший результат: -");
                            textViewLatest.setText("Последний результат: -");
                            break;
                        default:
                            bestLastResult.getResult(result);
                            textViewBest.setText("Лучший результат: " + bestLastResult.getBestResult());
                            textViewLatest.setText("Последний результат: " + bestLastResult.getLastResult());
                            break;
                    }
                    break;
            }

        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        System.gc();
    }

    @Override
    protected void onResume() {
        super.onResume();
        //убрать кнопки
        hideAppButtons();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personal_area);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        progressBar = (ProgressBar) findViewById(R.id.progressBarLoading);
        initViews();
        loadKolBirds();
        loadCurrentLogin();
        setLogin();
        spinner = (Spinner) findViewById(R.id.spinner);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            int idSelected = spinner.getSelectedItemPosition();

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                if (idSelected != position) {
                    Log.d("log", "I'm here");
                    if(!flag) {
                        task = "bestLastResult";
                        new RequestTask().execute();
                    }

                }
                hideAppButtons();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        task = "kolBirds";
        new RequestTask().execute();
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        switch (id) {
            case R.id.buttonBack:
                finish();
                break;

            case R.id.textViewTestName:
                break;
        }
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        int id;
        id = view.getId();
        switch (motionEvent.getAction()) {
            case MotionEvent.ACTION_DOWN:
                switch (id) {
                    case R.id.buttonBack:
                        buttonBack.setAlpha(0.6f);
                        break;
                }
                break;
            case MotionEvent.ACTION_UP:
                switch (id) {
                    case R.id.buttonBack:
                        buttonBack.setAlpha(1.0f);
                        break;
                }
                break;
        }
        return false;
    }

    //endregion
}
