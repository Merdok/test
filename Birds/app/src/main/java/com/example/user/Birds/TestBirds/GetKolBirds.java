package com.example.user.Birds.TestBirds;

import android.util.Log;

import org.apache.http.NameValuePair;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;

public class GetKolBirds extends Request {

    @Override
    public String createRequest() {
        try {
            //создаем запрос на сервер
            DefaultHttpClient hc = new DefaultHttpClient();
            ResponseHandler<String> res = new BasicResponseHandler();
            //создаем post запрос
            HttpPost postMethod = new HttpPost(URL);
            //список передаваемых параметров ключ - значение
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);

            //логин
            nameValuePairs.add(new BasicNameValuePair("login", login));
            //посылаем на сервер
            postMethod.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            //получаем ответ от сервера
            response = hc.execute(postMethod, res);

            return response;

        } catch (Exception e) {
            Log.d("Exp=", e + "");
            return "error";
        }
    }

    @Override
    public String getResult(String result) {
        String answer = result;
        try {
            //создаем json объект
            JSONObject json = new JSONObject(result);

            //дальше находим вход в наш json им является ключевое слово data
            JSONArray urls = json.getJSONArray("data");
            //проходим циклом по всем нашим параметрам
            for (int i = 0; i < urls.length(); i++) {
                answer = urls.getJSONObject(i).getString("kolBirds").toString();
            }
        } catch (JSONException e) {
            Log.e("log_tag", "Error parsing data " + e.toString() + answer);
            e.printStackTrace();
        }
        return answer;
    }
}
