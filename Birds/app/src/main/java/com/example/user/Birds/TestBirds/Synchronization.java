package com.example.user.Birds.TestBirds;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;


public class Synchronization {

    //region Свойства

    private AddResults addResults = new AddResults();
    private final String URL = "http://psytest.nstu.ru/t/birds/AddResults.php";
    private final String URL1 = "http://psytest.nstu.ru/t/birds/AddResultsToCommonTable.php";
    private Database database = new Database();

    //endregion

    //region Методы

    private String sendResultsFromTableInfoTest(Context context, int id, int points) {
        DBHelper db = new DBHelper(context);
        SQLiteDatabase database = db.getWritableDatabase();
        Cursor cursor;
        cursor = database.query(db.TABLE_NAME2, null, db.KEY_ID + " =?", new String[]{Integer.toString(id)}, null, null, null);
        if (cursor.moveToFirst()) {
            addResults.setURL(URL1);
            addResults.setLogin(cursor.getString(cursor.getColumnIndex(db.KEY_LOGIN)));
            addResults.add("login", addResults.getLogin());
            addResults.add("date", cursor.getString(cursor.getColumnIndex(db.KEY_DATE)));
            addResults.add("result", Integer.toString(points));
            addResults.add("version", "mobile version");
            addResults.add("testID", "9614");
            addResults.add("testName", "Тест с птицами");
            addResults.add("testNameProp", "Birds");
            cursor.close();
            db.close();
            return addResults.createRequest();
        }

        cursor.close();
        db.close();
        return "";
    }

    private String sendResultsFromTableResultsTest(Context context, int id, String commonTableID) {
        DBHelper db = new DBHelper(context);
        SQLiteDatabase database = db.getWritableDatabase();
        Cursor cursor;
        int kolBirds = this.database.getKolBirds(db, id), i = 0;
        cursor = database.query(db.TABLE_NAME1, null, db.KEY_ID + " =?", new String[]{Integer.toString(id)}, null, null, null);
        addResults.setURL(URL);
        addResults.setKolBirdsMax(kolBirds);
        addResults.add("kolBirds", Integer.toString(addResults.getKolBirdsMax()));
        addResults.add("id", commonTableID);
        if (cursor.moveToFirst()) {
            do {
                addResults.setLogin(cursor.getString(cursor.getColumnIndex(db.KEY_LOGIN)));
                addResults.setHit(cursor.getInt(cursor.getColumnIndex(db.KEY_HIT)));
                addResults.setDistractors(cursor.getInt(cursor.getColumnIndex(db.KEY_DISTRACTORS)));
                addResults.setDistance(cursor.getInt(cursor.getColumnIndex(db.KEY_DIST)));
                addResults.setPoints(cursor.getInt(cursor.getColumnIndex(db.KEY_POINTS)));
                addResults.setHitBird(cursor.getInt(cursor.getColumnIndex(db.KEY_HITBIRD)));
                addResults.add("login" + Integer.toString(i), addResults.getLogin());
                addResults.add("hit" + Integer.toString(i), Integer.toString(addResults.getHit()));
                addResults.add("distractors" + Integer.toString(i), Integer.toString(addResults.getDistractors()));
                addResults.add("distance" + Integer.toString(i), Integer.toString(addResults.getDistance()));
                addResults.add("points" + Integer.toString(i), Integer.toString(addResults.getPoints()));
                addResults.add("hitBird" + Integer.toString(i), Integer.toString(addResults.getHitBird()));
                i++;
            } while (cursor.moveToNext());

            cursor.close();
            db.close();
            return addResults.createRequest();
        }

        cursor.close();
        db.close();
        return "";

    }

    public String synchronize(Context context, ArrayList<Integer> indexes, String login) {
        String result = "empty";
        DBHelper db = new DBHelper(context);
        int points;
        for (int i = 0; i < indexes.size(); i++) {
            points = database.getSumPoints(db, indexes.get(i), login);
            result = sendResultsFromTableInfoTest(context, indexes.get(i), points);
            Log.d("sync", result);
            result = sendResultsFromTableResultsTest(context, indexes.get(i), result);
            Log.d("sync", result);
        }
        return result;
    }

    public void update(Context context, ArrayList<Integer> indexes) {
        DBHelper db = new DBHelper(context);
        for (int i = 0; i < indexes.size(); i++) {
            database.updateRecord(db, indexes.get(i), "Sync");
            database.deleteFromTable(db, db.TABLE_NAME2, indexes.get(i));
            database.deleteFromTable(db, db.TABLE_NAME1, indexes.get(i));
        }
    }

    //endregion
}
