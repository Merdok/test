package com.example.user.Birds.TestBirds;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;


public class Database {

    //region Свойства

    private final int MAX_RAWS = 20;

    //endregion

    //region Методы

    //region Функции добавления в таблицы

    public void addToTableUsers(DBHelper db, String login, String password) {

        SQLiteDatabase database = db.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        //кладем результат в таблицу
        contentValues.put(db.KEY_LOGIN, login);
        contentValues.put(db.KEY_PASSWORD, password);

        database.insert(db.TABLE_NAME, null, contentValues);
        db.close();
    }

    public void addToTableResultsTestBirds(DBHelper db, int id, String login, int hit, int hitBird, int distractors, int distance, int points, String mode) {

        SQLiteDatabase database = db.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        //кладем результат в таблицу
        contentValues.put(db.KEY_ID, id);
        contentValues.put(db.KEY_LOGIN, login);
        contentValues.put(db.KEY_HIT, hit);
        contentValues.put(db.KEY_HITBIRD, hitBird);
        contentValues.put(db.KEY_DISTRACTORS, distractors);
        contentValues.put(db.KEY_DIST, distance);
        contentValues.put(db.KEY_POINTS, points);
        contentValues.put(db.KEY_MODE, mode);

        database.insert(db.TABLE_NAME1, null, contentValues);
        db.close();
    }

    public void addToTableBestLastResultsBirds(DBHelper db, String login, int kolBirds, int points) {

        int best, id;
        SQLiteDatabase database = db.getWritableDatabase();
        Cursor cursor = database.query(db.TABLE_NAME3, null, db.KEY_LOGIN + "= ? AND " + db.KEY_KOLBIRDS + "= ?",
                new String[]{login, Integer.toString(kolBirds)}, null, null, null);

        if(cursor.moveToFirst()){
            best = cursor.getInt(cursor.getColumnIndex(db.KEY_BEST_RESULT));
            id = cursor.getInt(cursor.getColumnIndex(db.KEY_ID));
            if(best < points){
                best = points;
            }
            ContentValues contentValues = new ContentValues();
            //Обновляем результат в таблицу
            contentValues.put(db.KEY_LOGIN, login);
            contentValues.put(db.KEY_KOLBIRDS, kolBirds);
            contentValues.put(db.KEY_BEST_RESULT, best);
            contentValues.put(db.KEY_LAST_RESULT, points);
            database.update(db.TABLE_NAME3, contentValues, db.KEY_ID + " =?", new String[]{Integer.toString(id)});
        }else{
            ContentValues contentValues = new ContentValues();
            //кладем результат в таблицу
            contentValues.put(db.KEY_LOGIN, login);
            contentValues.put(db.KEY_KOLBIRDS, kolBirds);
            contentValues.put(db.KEY_BEST_RESULT, points);
            contentValues.put(db.KEY_LAST_RESULT, points);
            database.insert(db.TABLE_NAME3, null, contentValues);
            cursor.close();
            db.close();
        }


    }

    public void addToTableInfoTest(DBHelper db, String login, String date, String testName, String mode, String anons) {

        SQLiteDatabase database = db.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        //кладем результат в таблицу
        contentValues.put(db.KEY_LOGIN, login);
        contentValues.put(db.KEY_TEST, testName);
        contentValues.put(db.KEY_DATE, date);
        contentValues.put(db.KEY_MODE, mode);
        if (mode.equals("Online")) {
            contentValues.put(db.KEY_SYNC, "Sync");
        } else {
            contentValues.put(db.KEY_SYNC, "NoSync");
        }

        database.insert(db.TABLE_NAME2, null, contentValues);
        db.close();
    }

    public void addToTableResultsTestFigures(DBHelper db, int id, String login, String level, Long averageTimeWrong, Long averageTimeCorrect, Long averageTimeCorrectAfterWrong, Long averageDeviationWrong, Long averageDeviationCorrect, Long averageDeviationCorrectAfterWrong ){
        SQLiteDatabase database = db.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        //кладем результат в таблицу
        contentValues.put(db.KEY_ID, id);
        contentValues.put(db.KEY_LOGIN, login);
        contentValues.put(db.KEY_LEVEL, level);
        contentValues.put(db.KEY_AVERAGE_TIME_WRONG, averageTimeWrong);
        contentValues.put(db.KEY_AVERAGE_TIME_CORRECT, averageTimeCorrect);
        contentValues.put(db.KEY_AVERAGE_TIME_CORRECT_AFTER_WRONG, averageTimeCorrectAfterWrong);
        contentValues.put(db.KEY_AVERAGE_DEVIATION_WRONG, averageDeviationWrong);
        contentValues.put(db.KEY_AVERAGE_DEVIATION_CORRECT, averageDeviationCorrect);
        contentValues.put(db.KEY_AVERAGE_DEVIATION_CORRECT_AFTER_WRONG, averageDeviationCorrectAfterWrong);

        database.insert(db.TABLE_NAME4, null, contentValues);
        db.close();
    }

    public void addToTableBestLastResultsFigures(DBHelper db, String login, String level, int points) {

        int best, id;
        SQLiteDatabase database = db.getWritableDatabase();
        Cursor cursor = database.query(db.TABLE_NAME5, null, db.KEY_LOGIN + "= ? AND " + db.KEY_LEVEL + "= ?",
                new String[]{login, level}, null, null, null);

        if(cursor.moveToFirst()){
            best = cursor.getInt(cursor.getColumnIndex(db.KEY_BEST_RESULT));
            id = cursor.getInt(cursor.getColumnIndex(db.KEY_ID));
            if(best < points){
                best = points;
            }
            ContentValues contentValues = new ContentValues();
            //Обновляем результат в таблицу
            contentValues.put(db.KEY_LOGIN, login);
            contentValues.put(db.KEY_LEVEL, level);
            contentValues.put(db.KEY_BEST_RESULT, best);
            contentValues.put(db.KEY_LAST_RESULT, points);
            database.update(db.TABLE_NAME5, contentValues, db.KEY_ID + " =?", new String[]{Integer.toString(id)});
        }else{
            ContentValues contentValues = new ContentValues();
            //кладем результат в таблицу
            contentValues.put(db.KEY_LOGIN, login);
            contentValues.put(db.KEY_LEVEL, level);
            contentValues.put(db.KEY_BEST_RESULT, points);
            contentValues.put(db.KEY_LAST_RESULT, points);
            database.insert(db.TABLE_NAME5, null, contentValues);
            cursor.close();
            db.close();
        }


    }
    //endregion

    //region Функции вывода содержимого таблиц

    public void printTableUsers(DBHelper db) {
        SQLiteDatabase database = db.getWritableDatabase();
        Cursor cursor;

        cursor = database.query(db.TABLE_NAME, null, null, null, null, null, null);

        if (cursor.moveToFirst()) {

            do {
                Log.d("users", Integer.toString(cursor.getInt(cursor.getColumnIndex(db.KEY_ID))) + ", " +
                        cursor.getString(cursor.getColumnIndex(db.KEY_LOGIN)) + ", " + cursor.getString(cursor.getColumnIndex(db.KEY_PASSWORD)) + "\n");

            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();
    }

    public void printTableResultsTest(DBHelper db) {
        SQLiteDatabase database = db.getWritableDatabase();
        Cursor cursor;

        cursor = database.query(db.TABLE_NAME1, null, null, null, null, null, null);

        if (cursor.moveToFirst()) {

            do {
                Log.d("ResultsTest", cursor.getInt(cursor.getColumnIndex(db.KEY_ID)) + ", " +
                        cursor.getString(cursor.getColumnIndex(db.KEY_LOGIN)) + ", " + cursor.getInt(cursor.getColumnIndex(db.KEY_HIT)) + ", " +
                        cursor.getInt(cursor.getColumnIndex(db.KEY_HITBIRD)) + ", " + cursor.getInt(cursor.getColumnIndex(db.KEY_DIST)) + ", " +
                        cursor.getInt(cursor.getColumnIndex(db.KEY_POINTS)) + "," + cursor.getString(cursor.getColumnIndex(db.KEY_MODE)) + "\n");

            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();

    }

    public void printTableInfoTest(DBHelper db) {
        SQLiteDatabase database = db.getWritableDatabase();
        Cursor cursor;

        cursor = database.query(db.TABLE_NAME2, null, null, null, null, null, null);

        if (cursor.moveToFirst()) {

            do {
                Log.d("InfoTest", cursor.getInt(cursor.getColumnIndex(db.KEY_ID)) + ", " +
                        cursor.getString(cursor.getColumnIndex(db.KEY_LOGIN)) + ", " +
                        cursor.getString(cursor.getColumnIndex(db.KEY_TEST)) + "," + cursor.getString(cursor.getColumnIndex(db.KEY_MODE)) + "," +
                        cursor.getString(cursor.getColumnIndex(db.KEY_DATE)) + "," + cursor.getString(cursor.getColumnIndex(db.KEY_SYNC)) + "\n");

            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();
    }

    public void printTableResultsTestForUser(DBHelper db) {
        SQLiteDatabase database = db.getWritableDatabase();
        Cursor cursor;

        cursor = database.query(db.TABLE_NAME3, null, null, null, null, null, null);

        if (cursor.moveToFirst()) {

            do {
                Log.d("ResultsTestForUser", cursor.getInt(cursor.getColumnIndex(db.KEY_ID)) + ", " +
                        cursor.getString(cursor.getColumnIndex(db.KEY_LOGIN)) + ", " + cursor.getInt(cursor.getColumnIndex(db.KEY_KOLBIRDS)) + ", " +
                        cursor.getInt(cursor.getColumnIndex(db.KEY_BIRDS)) + ", " + cursor.getInt(cursor.getColumnIndex(db.KEY_NUMBER)) + ", " +
                        cursor.getInt(cursor.getColumnIndex(db.KEY_POINTS)) + "\n");

            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();
    }

    //endregion

    //region Функции получения данных из таблиц

    public int getBestResultFromTableBirds(DBHelper db, String login, int kolBirds) {
        SQLiteDatabase database = db.getWritableDatabase();
        Cursor cursor;

        cursor = database.query(db.TABLE_NAME3, null, db.KEY_LOGIN + "= ? AND " + db.KEY_KOLBIRDS + "= ?",
                new String[]{login, Integer.toString(kolBirds)}, null, null, null);
        if ( cursor.moveToFirst()) {
            db.close();
            int num = cursor.getInt(cursor.getColumnIndex(db.KEY_BEST_RESULT));
            cursor.close();
            return num;
        }
        cursor.close();
        db.close();
        return 0;
    }

    public int getBestResultFromTableFigures(DBHelper db, String login, String level){
        SQLiteDatabase database = db.getWritableDatabase();
        Cursor cursor;

        cursor = database.query(db.TABLE_NAME5, null, db.KEY_LOGIN + "= ? AND " + db.KEY_LEVEL + "= ?",
                new String[]{login, level}, null, null, null);
        if ( cursor.moveToFirst()) {
            db.close();
            int num = cursor.getInt(cursor.getColumnIndex(db.KEY_BEST_RESULT));
            cursor.close();
            return num;
        }
        cursor.close();
        db.close();
        return 0;
    }

    public int getLastResultFromTableBirds(DBHelper db, String login, int kolBirds) {
        SQLiteDatabase database = db.getWritableDatabase();
        Cursor cursor;

        cursor = database.query(db.TABLE_NAME3, null, db.KEY_LOGIN + "= ? AND " + db.KEY_KOLBIRDS + "= ?",
                new String[]{login, Integer.toString(kolBirds)}, null, null, null);

        if (cursor.moveToFirst()) {
            db.close();
            int num = cursor.getInt(cursor.getColumnIndex(db.KEY_LAST_RESULT));
            cursor.close();
            return num;
        }
        cursor.close();
        db.close();
        return 0;
    }

    public int getLastResultFromTableFigures(DBHelper db, String login, String level){
        SQLiteDatabase database = db.getWritableDatabase();
        Cursor cursor;

        cursor = database.query(db.TABLE_NAME5, null, db.KEY_LOGIN + "= ? AND " + db.KEY_LEVEL + "= ?",
                new String[]{login, level}, null, null, null);

        if (cursor.moveToFirst()) {
            db.close();
            int num = cursor.getInt(cursor.getColumnIndex(db.KEY_LAST_RESULT));
            cursor.close();
            return num;
        }
        cursor.close();
        db.close();
        return 0;
    }

    public String[] getLatestLoginFromTableUsers(DBHelper db, String login) {
        SQLiteDatabase database = db.getWritableDatabase();
        Cursor cursor;
        String logPass[] = new String[2];

        cursor = database.query(db.TABLE_NAME, null, db.KEY_LOGIN + "= ?", new String[]{login}, null, null, null);

        if (cursor.moveToFirst()) {
            logPass[0] = cursor.getString(cursor.getColumnIndex(db.KEY_LOGIN));
            logPass[1] = cursor.getString(cursor.getColumnIndex(db.KEY_PASSWORD));
        }
        else{
            logPass[0] = "No";
            logPass[1] = "No";
        }

        cursor.close();
        db.close();

        return logPass;
    }

    public int getIDFromTableInfoTest(DBHelper db, String login) {
        SQLiteDatabase database = db.getWritableDatabase();
        Cursor cursor;

        cursor = database.query(db.TABLE_NAME2, new String[]{db.KEY_ID}, db.KEY_LOGIN + " =?", new String[]{login}, null, null, null);

        if (cursor.moveToLast()) {
            int id = cursor.getInt(cursor.getColumnIndex(db.KEY_ID));
            cursor.close();
            db.close();
            return id;
        }

        cursor.close();
        db.close();
        return -1;
    }

    public int getSumPoints(DBHelper db, int id, String login) {
        SQLiteDatabase database = db.getWritableDatabase();
        Cursor cursor;

        cursor = database.query(db.TABLE_NAME1, new String[]{"SUM(" + db.KEY_POINTS + ") AS SUMMA"}, db.KEY_LOGIN + " =? AND " + db.KEY_ID + " =?",
                new String[]{login, Integer.toString(id)}, null, null, null);

        if (cursor.moveToFirst()) {
            int sum = cursor.getInt(cursor.getColumnIndex("SUMMA"));
            cursor.close();
            db.close();
            return sum;
        }

        cursor.close();
        db.close();
        return -1;
    }

    public int getFirstIDFromInfoTest(DBHelper db) {

        SQLiteDatabase database = db.getWritableDatabase();
        Cursor cursor;

        cursor = database.query(db.TABLE_NAME2, null, null, null, null, null, null);

        if (cursor.moveToFirst()) {
            int id = cursor.getInt(cursor.getColumnIndex(db.KEY_ID));
            cursor.close();

            return id;
        }

        cursor.close();
        return -1;
    }

    public int getLastIDFromInfoTest(DBHelper db) {
        SQLiteDatabase database = db.getWritableDatabase();
        Cursor cursor;

        cursor = database.query(db.TABLE_NAME2, null, null, null, null, null, null);

        if (cursor.moveToLast()) {
            int id = cursor.getInt(cursor.getColumnIndex(db.KEY_ID));
            cursor.close();
            db.close();
            return id;
        }

        cursor.close();
        db.close();
        return -1;
    }

    public ArrayList<Integer> getNoSyncIndexesFromInfoTest(DBHelper db, String login) {
        ArrayList<Integer> indexes = new ArrayList<Integer>();
        if (!login.equals("an")) {
            SQLiteDatabase database = db.getWritableDatabase();
            Cursor cursor;
            String res;
            cursor = database.query(db.TABLE_NAME2, null, db.KEY_LOGIN + " =?", new String[]{login}, null, null, null);

            if (cursor.moveToFirst()) {
                do {
                    res = cursor.getString(cursor.getColumnIndex(db.KEY_SYNC));
                    if (res.equals("NoSync")) {
                        indexes.add(cursor.getInt(cursor.getColumnIndex(db.KEY_ID)));
                        Log.d("indexNoSync", cursor.getInt(cursor.getColumnIndex(db.KEY_ID)) + "");
                    }
                } while (cursor.moveToNext());
            }

            cursor.close();
            db.close();
            return indexes;
        }

        return indexes;
    }

    public ArrayList<Integer> getKolBirds(DBHelper db, String login){

        ArrayList<Integer> kolBirds = new ArrayList<>();
        SQLiteDatabase database = db.getWritableDatabase();
        Cursor cursor;
        cursor = database.query(true, db.TABLE_NAME3, new String[]{db.KEY_KOLBIRDS}, db.KEY_LOGIN + " =?",
                new String[]{login}, null, null, db.KEY_KOLBIRDS + " ASC", null);
        if(cursor.moveToFirst()){
            do{
                kolBirds.add(cursor.getInt(cursor.getColumnIndex(db.KEY_KOLBIRDS)));
            }while (cursor.moveToNext());

            cursor.close();
            db.close();
            return kolBirds;
        }

        //признак того, что таблица пуста
        kolBirds.add(0);
        cursor.close();
        db.close();
        return kolBirds;
    }

    public int getKolBirds(DBHelper db, int id) {
        int kolBirds;
        SQLiteDatabase database = db.getWritableDatabase();
        Cursor cursor;
        cursor = database.query(db.TABLE_NAME1, null, db.KEY_ID + " =?", new String[]{Integer.toString(id)}, null, null, null);
        kolBirds = cursor.getCount();
        return kolBirds;
    }

    public int getIDBestResult(DBHelper db, String login, int kolBirds) {
        SQLiteDatabase database = db.getWritableDatabase();
        Cursor cursor;
        cursor = database.query(db.TABLE_NAME3, null, db.KEY_LOGIN + " =? AND " + db.KEY_KOLBIRDS + " =?", new String[]{login, Integer.toString(kolBirds)},
                null, null, db.KEY_POINTS + " desc");
        if (cursor.moveToFirst()) {
            int id = cursor.getInt(cursor.getColumnIndex(db.KEY_ID));
            cursor.close();
            db.close();
            return id;
        }

        cursor.close();
        db.close();
        return 0;
    }

    public String getDate(DBHelper db, int id){
        SQLiteDatabase database = db.getWritableDatabase();
        Cursor cursor;
        String date = "";
        cursor = database.query(db.TABLE_NAME2, null, db.KEY_ID + " =?", new String[]{Integer.toString(id)},
                null, null, null);
        if (cursor.moveToFirst()){
            date = cursor.getString(cursor.getColumnIndex(db.KEY_DATE));
        }
        cursor.close();
        db.close();
        return date;

    }
    //endregion

    //region Проверки

    public boolean IsSyncID(DBHelper db, int id) {
        SQLiteDatabase database = db.getWritableDatabase();
        Cursor cursor;
        cursor = database.query(db.TABLE_NAME2, null, db.KEY_ID + " =?", new String[]{Integer.toString(id)}, null, null, null);
        if (cursor.moveToFirst()) {
            if (cursor.getString(cursor.getColumnIndex(db.KEY_SYNC)).equals("Sync")) {
                cursor.close();
                db.close();
                return true;
            }
        }

        cursor.close();
        db.close();
        return false;
    }

    //endregion

    //region Функции обновления данных в таблице

    public void updateRecord(DBHelper db, int id, String sync){
        SQLiteDatabase database = db.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        Cursor cursor;
        cursor = database.query(db.TABLE_NAME2, null, db.KEY_ID + " =?", new String[]{Integer.toString(id)}, null, null, null);
        if (cursor.moveToFirst()) {
            contentValues.put(db.KEY_LOGIN, cursor.getString(cursor.getColumnIndex(db.KEY_LOGIN)));
            contentValues.put(db.KEY_TEST, cursor.getString(cursor.getColumnIndex(db.KEY_TEST)));
            contentValues.put(db.KEY_MODE, cursor.getString(cursor.getColumnIndex(db.KEY_MODE)));
            contentValues.put(db.KEY_SYNC, sync);
            cursor.close();
            database.update(db.TABLE_NAME2, contentValues, db.KEY_ID + " =?", new String[]{Integer.toString(id)});
        }
        printTableInfoTest(db);
        db.close();

    }

    //endregion

    //region Функции удаления данных из таблиц

    public void deleteFromTable(DBHelper db, final String TABLE_NAME, int id) {

        SQLiteDatabase database = db.getWritableDatabase();
        database.delete(TABLE_NAME, db.KEY_ID + " =?", new String[]{Integer.toString(id)});
        db.close();

    }

    //endregion

    //endregion
}
