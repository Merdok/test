package com.example.user.Birds;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Toast;

import com.example.user.Birds.TestBirds.Loader;

public class SettingsActivity extends PreferenceActivity {

    //region Свойства
    SharedPreferences prefs;
    private final String KOL_BIRDS = "kol_stim";
    private final String TIME_MAX = "time_max";
    private final String TIME_MIN = "time_min";
    private final String TIME_STEP = "time_step";
    private final String TIME_BETWEEN_BIRDS = "time_between_birds";
    private final String KOL_DIST = "kol_dist";

    String kolStim;
    String kolDist;
    String timeMax;
    String timeMin;
    String timeStep;
    String timeBetweenStim;

    Toast toast;

    Loader loader = new Loader();

    //endregion

    //region Методы

    public void hideAppButtons() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT)
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
    }

    //region Загрузки

    public void loadParam(){
        loader.load(this, "Settings", KOL_BIRDS, "10");
        kolStim = loader.getValue();

        loader.load(this, "Settings", KOL_DIST, "3");
        kolDist= loader.getValue();

        loader.load(this, "Settings", TIME_MAX, "1000");
        timeMax = loader.getValue();

        loader.load(this, "Settings", TIME_MIN, "100");
        timeMin= loader.getValue();

        loader.load(this, "Settings", TIME_STEP, "100");
        timeStep = loader.getValue();

        loader.load(this, "Settings", TIME_BETWEEN_BIRDS, "1000");
        timeBetweenStim = loader.getValue();
    }

    //endregion

    //region Set-функции

    public void setTitle(String title, String value, String key) {
        Preference pref = findPreference(key);
        pref.setTitle(title + " " + value);
    }

    //endregion

    //region Переопределенные функции Activity

    @Override
    protected void onDestroy() {
        super.onDestroy();
        System.gc();
    }

    @Override
    protected void onResume() {
        super.onResume();
        hideAppButtons();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.settings);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        loadParam();
        Context context = getApplicationContext();
        prefs = PreferenceManager.getDefaultSharedPreferences(context);
        Preference buttonExit = (Preference) getPreferenceManager().findPreference("exit");
        if (buttonExit != null) {
            buttonExit.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                @Override
                public boolean onPreferenceClick(Preference arg0) {
                    finish();
                    return true;
                }
            });
        }

        setTitle("Количество стимулов:", kolStim, KOL_BIRDS);
        setTitle("Время показа стимула (Макс.):", timeMax, TIME_MAX);
        setTitle("Время показа стимула (Мин.):", timeMin, TIME_MIN);
        setTitle("Время показа стимула (Шаг):", timeStep, TIME_STEP);
        setTitle("Количество дистракторов:", kolDist, KOL_DIST);
        setTitle("Время между предъявлениями:", timeBetweenStim, TIME_BETWEEN_BIRDS);
    }

    //endregion

    //endregion
}

