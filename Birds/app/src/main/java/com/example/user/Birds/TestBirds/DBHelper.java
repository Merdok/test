package com.example.user.Birds.TestBirds;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by User on 23.04.2016.
 */
public class DBHelper extends SQLiteOpenHelper {

    //region Свойства

    public static final String DATABASE_NAME = "userDB";
    public static final int DATABASE_VERSION = 1;
    public static final String TABLE_NAME = "users";
    public static final String TABLE_NAME1 = "birdsTestResults";
    public static final String TABLE_NAME2 = "InfoTest";
    public static final String TABLE_NAME3 = "birdsBestLastResults";
    public static final String TABLE_NAME4 = "figuresTestResults";
    public static final String TABLE_NAME5 = "figuresBestLastResults";

    // поля для таблицы USERS логин-пароль
    public static final String KEY_ID = "id";
    public static final String KEY_LOGIN = "login";
    public static final String KEY_PASSWORD = "password";

    // поля для таблицы с результатами теста (тест Птицы)
    public static final String KEY_BIRDS = "birds";
    public static final String KEY_NUMBER = "numbers";
    public static final String KEY_POINTS = "points";
    public static final String KEY_KOLBIRDS = "kolBrids";
    public static final String KEY_HIT = "hits";
    public static final String KEY_HITBIRD = "hitBird";
    public static final String KEY_DIST = "distance";
    public static final String KEY_DISTRACTORS = "distractors";
    public static final String KEY_DATE = "date";
    public static final String KEY_TEST = "testName";
    public static final String KEY_MODE = "mode";
    public static final String KEY_SYNC = "synchronize";

    // поля для таблицы с результатами теста (тест Фигуры)
    public static final String KEY_LEVEL = "level";
    public static final String KEY_AVERAGE_TIME_WRONG = "averageTimeWrongAnswers";
    public static final String KEY_AVERAGE_TIME_CORRECT = "averageTimeCorrectAnswers";
    public static final String KEY_AVERAGE_TIME_CORRECT_AFTER_WRONG = "averageTimeCorrectAfterWrongAnswers";
    public static final String KEY_AVERAGE_DEVIATION_WRONG = "averageDeviationWrongAnswers";
    public static final String KEY_AVERAGE_DEVIATION_CORRECT = "averageDeviationCorrectAnswers";
    public static final String KEY_AVERAGE_DEVIATION_CORRECT_AFTER_WRONG = "averageDeviationCorrectAfterWrongAnswers";
    public static final String KEY_BEST_RESULT = "bestResult";
    public static final String KEY_LAST_RESULT = "lastResult";
    //endregion

    //region Методы

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        createTable(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
       deleteTable(TABLE_NAME, db);
        onCreate(db);
    }

    public void createTable(SQLiteDatabase db){
        // таблица сохраненных логинов и паролей
        db.execSQL("create table " + TABLE_NAME + "(" + KEY_ID + " integer primary key AUTOINCREMENT,"
                + KEY_LOGIN + " text," + KEY_PASSWORD + " text" + ")");

        // локальная таблица для Хранения лучшего и последнего результата (тест Птицы)
        db.execSQL("create table " + TABLE_NAME3 + "(" + KEY_ID + " integer primary key AUTOINCREMENT," + KEY_LOGIN + " text,"
                + KEY_KOLBIRDS + " integer," +  KEY_BEST_RESULT + " integer," + KEY_LAST_RESULT + " integer" + ")");

        // локальная таблица для Хранения лучшего и последнего результата (тест Фигуры)
        db.execSQL("create table " + TABLE_NAME5 + "(" + KEY_ID + " integer primary key AUTOINCREMENT," + KEY_LOGIN + " text,"
                 +  KEY_LEVEL + " text," + KEY_BEST_RESULT + " integer," + KEY_LAST_RESULT + " integer" + ")");


        // локальная таблица с полными результатами (тест Птицы)
        db.execSQL("create table " + TABLE_NAME1 + "(" + KEY_ID + " integer," + KEY_LOGIN + " text,"
                + KEY_HIT + " integer," + KEY_HITBIRD + " integer," + KEY_DISTRACTORS + " integer,"
                + KEY_DIST + " integer," + KEY_POINTS + " integer," + KEY_MODE + " text" + ")");

        // локальная таблица с полными результатами (тест Фигуры)
        db.execSQL("create table " + TABLE_NAME4 + "(" + KEY_ID + " integer," + KEY_LOGIN + " text,"
                + KEY_LEVEL + " text," + KEY_AVERAGE_TIME_WRONG + " integer," + KEY_AVERAGE_TIME_CORRECT + " integer,"
                + KEY_AVERAGE_TIME_CORRECT_AFTER_WRONG + " integer," + KEY_AVERAGE_DEVIATION_WRONG + " integer," + KEY_AVERAGE_DEVIATION_CORRECT + " integer,"
        + KEY_AVERAGE_DEVIATION_CORRECT_AFTER_WRONG + " integer" + ")");

        // таблица с информацией кто и когда проходил тест
        db.execSQL("create table " + TABLE_NAME2 + "(" + KEY_ID + " integer primary key AUTOINCREMENT," + KEY_LOGIN + " text,"
         + KEY_TEST + " text," + KEY_DATE + " text," + KEY_MODE + " text," + KEY_SYNC + " text" + ")");

    }

    public void deleteTable(final String TABLE_NAME, SQLiteDatabase db){
        db.execSQL("drop table if exists " + TABLE_NAME);
    }

    //endregion
}
