package com.example.user.Birds;

import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.Birds.TestBirds.GetBestResultsTable;

public class BestResultsTableActivity extends AppCompatActivity implements View.OnClickListener, View.OnTouchListener {

    ArrayAdapter<String> adapterSpinner;

    ArrayAdapter<String> adapterListView;

    ProgressBar progressBar;

    Button buttonBack;

    TextView textView, titleTable;

    Spinner spinner;

    ListView listView;

    String[] kolBirds = {"5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15"};

    String[] loginBestResult;

    String URL = "http://psytest.nstu.ru/MobileBirds/sendTable.php";

    GetBestResultsTable getBestResultsTable = new GetBestResultsTable();

    boolean clickTitleTable = false;


    public void hideAppButtons() {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT)
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
    }

    public void createAdapterSpinner() {
        adapterSpinner = new ArrayAdapter<String>(this, R.layout.row, R.id.sp, kolBirds);
        spinner.setAdapter(adapterSpinner);
    }

    public void createAdapterListView() {
        adapterListView = new ArrayAdapter<String>(this, R.layout.row, R.id.sp, loginBestResult);
        listView.setAdapter(adapterListView);
    }

    public int getSize(String string) {
        int size = 0;
        for (int i = 0; i < string.length(); i++) {
            if (string.charAt(i) == '(')
                size++;
        }

        Log.d("size", size + "");
        return size;
    }

    public void parserTable(String string) {

        loginBestResult = new String[getSize(string)];
        int num = 0;

        for (int i = 0; i < string.length(); i++) {
            if (string.charAt(i) == '(') {
                int j = i + 1;
                String str = Integer.toString(num + 1) + ". ";
                while (string.charAt(j) != ')') {
                    if (string.charAt(j) == ':') {
                        str += " - ";
                    } else {
                        str += Character.toString(string.charAt(j));
                    }
                    j++;
                }

                loginBestResult[num] = str;
                num++;
            }


        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        //убрать кнопки
        hideAppButtons();
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        switch (id) {

            case R.id.buttonBack:
                finish();
                break;

            case R.id.titleTable:
                clickTitleTable = !clickTitleTable;
                if(clickTitleTable){
                    titleTable.setText("Таблица рекордов(Site)");
                    URL = "http://psytest.nstu.ru/MobileBirds/sendTableFromSite.php";
                    new RequestTask().execute();
                }else{
                    titleTable.setText("Таблица рекордов(Mobile)");
                    URL = "http://psytest.nstu.ru/MobileBirds/sendTable.php";
                    new RequestTask().execute();
                }

                break;
        }
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        int id;
        id = view.getId();
        switch (motionEvent.getAction()) {
            case MotionEvent.ACTION_DOWN:
                switch (id) {
                    case R.id.buttonBack:
                        buttonBack.setAlpha(0.6f);
                        break;
                }
                break;
            case MotionEvent.ACTION_UP:
                switch (id) {
                    case R.id.buttonBack:
                        buttonBack.setAlpha(1.0f);
                        break;
                }
                break;
        }
        return false;
    }

    class RequestTask extends AsyncTask<Void, Void, String> {
        @Override
        protected void onPreExecute() {
            progressBar.setVisibility(View.VISIBLE);
            getBestResultsTable.setURL(URL);
            getBestResultsTable.setKolBirds(spinner.getSelectedItem().toString());
            textView.setVisibility(View.INVISIBLE);
        }

        @Override
        protected String doInBackground(Void... params) {
            String str = "";
            str = getBestResultsTable.createRequest();
            return str;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Log.d("res", result);
            progressBar.setVisibility(View.INVISIBLE);
            if (!result.equals("error"))
                result = getBestResultsTable.getResult(result);
            else {
                Toast.makeText(BestResultsTableActivity.this, "Проблемы с соединением.", Toast.LENGTH_SHORT).show();
            }
            switch (result) {

                case "":
                    parserTable(result);
                    createAdapterListView();
                    textView.setVisibility(View.VISIBLE);
                    break;

                default:
                    Log.d("res", result);
                    parserTable(result);
                    createAdapterListView();
                    break;
            }

        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_best_results_table);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        spinner = (Spinner) findViewById(R.id.spinner2);
        listView = (ListView) findViewById(R.id.listView);
        textView = (TextView) findViewById(R.id.textView);
        titleTable = (TextView) findViewById(R.id.titleTable);
        titleTable.setOnClickListener(this);
        buttonBack = (Button) findViewById(R.id.buttonBack);
        buttonBack.setOnClickListener(this);
        buttonBack.setOnTouchListener(this);
        createAdapterSpinner();
        progressBar = (ProgressBar) findViewById(R.id.progressBarLoading);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            int idSelected = spinner.getSelectedItemPosition();

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                if (idSelected != position) {
                    new RequestTask().execute();
                    idSelected = position;
                }
                hideAppButtons();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        new RequestTask().execute();
    }
}
