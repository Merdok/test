package com.example.user.Birds;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class InstructionActivity extends AppCompatActivity implements View.OnClickListener, View.OnTouchListener {

    //region Свойства

    Button buttonBack, buttonPlay, buttonSettings, buttonAuth;

    TextView textView;

    RelativeLayout relativeLayout;

    boolean show = false;

    //endregion

    //region Методы

    public void hideButtons(){
        buttonPlay.setVisibility(View.INVISIBLE);
        buttonSettings.setVisibility(View.INVISIBLE);
        buttonAuth.setVisibility(View.INVISIBLE);
    }

    //region Переопределенные функции Activity

    @Override
    protected void onDestroy() {
        super.onDestroy();
        System.gc();
    }

    @Override
    protected void onResume() {
        super.onResume();
        //убрать кнопки
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT)
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent(this, MenuActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_instruction);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        buttonBack = (Button) findViewById(R.id.back);
        buttonBack.setOnClickListener(this);
        buttonBack.setOnTouchListener(this);

        buttonPlay = (Button) findViewById(R.id.buttonPlay);
        buttonPlay.setOnClickListener(this);
        buttonPlay.setOnTouchListener(this);

        buttonSettings = (Button) findViewById(R.id. buttonSettings);
        buttonSettings.setOnClickListener(this);
        buttonSettings.setOnTouchListener(this);

        buttonAuth = (Button) findViewById(R.id.buttonAuth);
        buttonAuth.setOnClickListener(this);
        buttonAuth.setOnTouchListener(this);

        relativeLayout = (RelativeLayout) findViewById(R.id.relative_layout);

        textView = (TextView) findViewById(R.id.textViewDescription);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        switch (id){
            case R.id.back:
                if(!show) {
                    Intent intent = new Intent(this, MenuActivity.class);
                    startActivity(intent);
                    finish();
                }else{
                    textView.setVisibility(View.INVISIBLE);
                    buttonPlay.setVisibility(View.VISIBLE);
                    buttonSettings.setVisibility(View.VISIBLE);
                    buttonAuth.setVisibility(View.VISIBLE);
                    show = false;
                }
                break;

            case R.id.buttonPlay:
                show = true;
                textView.setText(R.string.howToPlay);
                textView.setVisibility(View.VISIBLE);
                hideButtons();
                break;

            case R.id.buttonSettings:
                show = true;
                textView.setText(R.string.aboutSettings);
                textView.setVisibility(View.VISIBLE);
                hideButtons();
                break;

            case R.id.buttonAuth:
                show = true;
                textView.setText(R.string.aboutAuth);
                textView.setVisibility(View.VISIBLE);
                hideButtons();
                break;
        }
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        int id;
        id = view.getId();
        switch (motionEvent.getAction()) {
            case MotionEvent.ACTION_DOWN:
                switch (id) {
                    case R.id.back:
                        buttonBack.setAlpha(0.6f);
                        break;

                    case R.id.buttonPlay:
                        buttonPlay.setAlpha(0.6f);
                        break;

                    case R.id.buttonSettings:
                        buttonSettings.setAlpha(0.6f);
                        break;

                    case R.id.buttonAuth:
                        buttonAuth.setAlpha(0.6f);
                        break;
                }
                break;
            case MotionEvent.ACTION_UP:
                switch (id) {
                    case R.id.back:
                        buttonBack.setAlpha(1.0f);
                        break;

                    case R.id.buttonPlay:
                        buttonPlay.setAlpha(1.0f);
                        break;

                    case R.id.buttonSettings:
                        buttonSettings.setAlpha(1.0f);
                        break;

                    case R.id.buttonAuth:
                        buttonAuth.setAlpha(1.0f);
                        break;
                }
                break;


        }
        return false;
    }

    //endregion

    //endregion
}
