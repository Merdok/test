package com.example.user.Birds.TestBirds;

import android.util.Log;

import org.apache.http.NameValuePair;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class AddResults extends Request {

    //region Свойства

    private int hit;

    private int distance;

    private int distractors;

    private int points;

    private int kolBirdsMax;

    private int hitBird;

    private List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);

    //endregion

    //region Методы

    //region Переопределенные функции Request

    @Override
    public String createRequest() {
        try {
            //создаем запрос на сервер
            DefaultHttpClient hc = new DefaultHttpClient();
            ResponseHandler<String> res = new BasicResponseHandler();
            //создаем post запрос
            HttpPost postMethod = new HttpPost(URL);
            //передаем параметры
            postMethod.setEntity(new UrlEncodedFormEntity(nameValuePairs, HTTP.UTF_8));
            //получаем ответ от сервера
            response = hc.execute(postMethod, res);

            nameValuePairs.clear();

            return response;

        } catch (Exception e) {
            Log.d("Exp=", e + "");
            return "error";
        }
    }

    @Override
    public String getResult(String result) {
        String answer = "";
        try {
            //создали читателя json объектов и отдали ему строку - result
            JSONObject json = new JSONObject(result);


            //дальше находим вход в наш json им является ключевое слово data
            JSONArray urls = json.getJSONArray("data");
            //проходим циклом по всем нашим параметрам
            for (int i = 0; i < urls.length(); i++) {
                //читаем что в себе хранит параметр answer
                Log.d("Name\n", urls.getJSONObject(i).getString("answer").toString());
                answer = urls.getJSONObject(i).getString("answer").toString();
            }
        } catch (JSONException e) {
            Log.e("log_tag", "Error parsing data " + e.toString() + answer);
            e.printStackTrace();
        }
        return answer;
    }

    public void add(String key, String value){
        nameValuePairs.add(new BasicNameValuePair(key, value));
    }

    //endregion

    //region Set-функции

    public void setHit(int hit) {
        this.hit = hit;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public void setDistractors(int distractors) {
        this.distractors = distractors;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public void setKolBirdsMax(int kolBirdsMax) {
        this.kolBirdsMax = kolBirdsMax;
    }

    public void setHitBird(int hitBird) {
        this.hitBird = hitBird;
    }

    //endregion

    //region Get-функции

    public List<NameValuePair> getNameValuePairs(){
        return this.nameValuePairs;
    }

    public int getHit() {
        return this.hit;
    }

    public int getDistance() {
        return this.distance;
    }

    public int getDistractors() {
        return this.distractors;
    }

    public int getPoints() {
        return this.points;
    }

    public int getKolBirdsMax() {
        return this.kolBirdsMax;
    }

    public int getHitBird(){
        return this.hitBird;
    }

    //endregion

    //endregion
}
